# -*- coding: utf-8 -*-
"""
Created on Sat Dec  7 23:21:04 2019.

@author: bourd
"""

import pickle
from Datas import DaS
import copy
from Configuration import distrib_filename, weight_t, weight_m
import numpy as np
from Custom_RF import ForestUtils
from GraphicReport import ReportsUtil


class LTM(object) :
    '''
    SINGLETON

    Attributes
    ----------
    explicite : Explicite_LTM
    implicite : Implicite_LTM
    '''
    class __LTM:

        def __init__(self, forest = None):

            print("LTM __init__")
            self.__implicite = Implicite_LTM()
            self.__explicite = Explicite_LTM()

            cVal = DaS().getCVal()
            self.tValConso = cVal[0]
            self.mValConso = cVal[1]

            self.cTest = DaS().getCTest()

            print("\tCalculating initial performances ...")
            typo_test_data_counts = np.unique(self.cTest[0][1], return_counts = True)[1]
            manu_test_data_counts = np.unique(self.cTest[1][1], return_counts = True)[1]
            self.classes_weights_typo = typo_test_data_counts/sum(typo_test_data_counts)
            self.classes_weights_manu = manu_test_data_counts/sum(manu_test_data_counts)

            self.__implicite.RF = ForestUtils.wrapperSBSmethod(forest, self.tValConso[0],
                                                            self.tValConso[1], self.mValConso[0],
                                                            self.mValConso[1], weight_t, weight_m)
            (self.__implicite.sbs_rf_score_t_class, self.__implicite.sbs_rf_score_m_class) = ForestUtils.get_accuracy_score_by_class(self.__implicite.RF,
                                                                                                             self.tValConso[0],
                                                                                                             self.tValConso[1],
                                                                                                             self.mValConso[0],
                                                                                                             self.mValConso[1])
            self.__implicite.sbs_rf_score_t = np.average(self.__implicite.sbs_rf_score_t_class, weights = self.classes_weights_typo )
            self.__implicite.sbs_rf_score_m = np.average(self.__implicite.sbs_rf_score_m_class, weights = self.classes_weights_manu )

            print("\tImplicite __init__: OK")


        def consolidate(self, best_stm):

            T = len(self.tValConso[1])
            M = len(self.mValConso[1])

            print("LTM __consolidate__: applying sbs & calculating scores for comparison")

            best_stm_sbs = ForestUtils.wrapperSBSmethod(self.getImpliciteRF(), self.tValConso[0],
                                                            self.tValConso[1], self.mValConso[0],
                                                            self.mValConso[1], weight_t, weight_m)

            sbs_stm_perf_t = best_stm_sbs.score(self.tValConso[0], self.tValConso[1])
            print("\tTypo cVal : size=", T)
            print("\t\tSBS(Best STM) score =", sbs_stm_perf_t)
            print("\t\tImplicite RF score =", self.__implicite.sbs_rf_score_t)

            sbs_stm_perf_m = best_stm_sbs.score(self.mValConso[0], self.mValConso[1])
            print("\tManu cVal: size=", M)
            print("\t\tSBS(Best STM) score =", sbs_stm_perf_m)
            print("\t\tImplicite RF score =", self.__implicite.sbs_rf_score_m)


            if (sbs_stm_perf_t * weight_t + sbs_stm_perf_m * weight_m )/(weight_t + weight_m)  > \
                (self.__implicite.sbs_rf_score_t * weight_t + self.__implicite.sbs_rf_score_m * weight_m)/(weight_t + weight_m) :

                self.__implicite.RF = copy.deepcopy(best_stm_sbs)
                self.__implicite.sbs_rf_score_t = sbs_stm_perf_t
                self.__implicite.sbs_rf_score_m = sbs_stm_perf_m
                print("\tLTM __consolidate__: Implicite_LTM's RF upgraded")

            (perf_class_t_test, perf_class_m_test) = ForestUtils.get_accuracy_score_by_class(self.__implicite.RF,
                                                                                            self.tValConso[0],
                                                                                            self.tValConso[1],
                                                                                            self.mValConso[0],
                                                                                            self.mValConso[1])

            ReportsUtil().current_report.addConsolidationCycleResult(self.__implicite.RF, perf_class_t_test, perf_class_m_test)



            print("\t\tTypo cTest : size=", len(self.cTest[0][1]))
            print("\t\t\tImplicite RF score =", self.__implicite.RF.score(self.cTest[0][0], self.cTest[0][1]))

            print("\t\tManu cTest: size=", len(self.cTest[1][1]))

            print("\t\t\tImplicite RF score =", self.__implicite.RF.score(self.cTest[1][0], self.cTest[1][1]))



        def getTypoDistribution(self):
            return copy.deepcopy(self.__explicite.typo_dist)

        def getManuDistribution(self):
            return copy.deepcopy(self.__explicite.manu_dist)

        def getImpliciteRF(self):
            return  copy.deepcopy(self.__implicite.RF)
        def getTypoScore(self):
            return self.__implicite.sbs_rf_score_t_class
        def getManuScore(self):
            return self.__implicite.sbs_rf_score_m_class

        def reset(self, forest):
            self.__init__(forest)


    instance = None

    def __new__(self, forest = None, force_init = False):
        if force_init or not LTM.instance:
                LTM.instance = LTM.__LTM(forest)

        return LTM.instance







class Implicite_LTM:
    """
    Attributes
    ----------
    RF : CustomRF
    """
    def __init__(self):

        self.RF = None
        self.sbs_rf_score_t = None
        self.sbs_rf_score_m = None
        self.sbs_rf_score_t_class = None
        self.sbs_rf_score_m_class = None



def LoadTypoDistribution():
    with open(distrib_filename, 'rb') as f:
        return pickle.load(f)

class Explicite_LTM:
    """
    Attributes
    ----------
    dist : dict ( keys = [ n_classes] )
        dist[c] = [[mean_0 , ....n mean_255],Cov]
        with   Cov = [ var_0, ..., var_255] if Cov is diag
          else Cov = [ [ var_0 , var_0_1, ....., var_0_255],
                                    ...
                       [ var_255_0 , var_255_1, ....., var_255]]

    n_classes : int
        The number of data classes

    n_features : int
        The number of data features

    **_n_samples_classes : array ( shape = [ n_classes ] )
        ** = typo :  n_samples_classes[i] = cste = 6144
        ** =  manu : init = 0 then variable
    """

    def __init__(self):
        self.typo_dist = LoadTypoDistribution()
        print("\tExplicite __init__: OK")
