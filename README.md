# Complementary Learning System (CLS)
>Auteurs : Bérangère BOURDOULOUS & Mélanie PIOT

Références : https://github.com/scikit-learn/scikit-learn/tree/b194674c42d54b26137a456c510c5fdba1ba23e0/sklearn 

Cette documentation contient 4 guides :

- Guide d'installation
- Guide de construction des datasets
- Guide des classes
- Guide d'utilisation

----
##  1 - GUIDE D'INSTALLATION
----

Importer **cls** et **cls_datasets** (gitlab séparé) et placez les côte à côte.
### **1.1 - Configuration Python (à compléter / corriger)**

Liste des packages nécessaires : 

>- tensorflow, keras
>- scikit-learn, scipy
>- cython, openpyxl
>- others ...

### **1.2 - Configuration Cython (à compléter / corriger)**

*A) Installer un compilateur C*

- Sous OS linux utiliser simplement gcc.

- Sous OS Windows installer Visual Studio en incluant le compilateur C.

*B) Compiler la classe MyTree*
Tester la configuration en créant un nouvel objet MyTree en ligne de commande (**après avoir compilé le fichier CustomDecisionTreeClassifier.py**). Si la classe MyTree est introuvable alors la compilation n'a pas réussi.

Dans ce cas lancez la commande ```python setup.py install``` dans une console conda puis réessayez la manipulation précédente.

>Note : Si l'erreur ```Windows : Unable to find vcvarsall.bat ``` s'affiche dans la console Python c'est que soit la case du compilateur C n'a pas été cochée au moment de l'installation, soit que la version de VS n'est pas compatible avec votre version de Windows ou de Python. 

Remarque : actuellement sur Spyder4 il ne semble pas nécessaire de posséder les fichiers ```mytree.h```, ```mytree.c``` ou le dossier ```build``` pour faire fonctionner le projet.


##  2 - GUIDE DE CONSTRUCTION DES DATASETS
----
### **2.1 - Lire et stocker dans un numpy array les images (ex : JPEG) d'un dossier local**

### **2.2 - Sauvegarder un dataset d'image dans 2 fichiers binaires (data + labels)**

### **2.3 - Importer MNIST**

### **2.4 - Convertir les images en 256 dimensions (CNN + ACP)**

### **2.5 - Estimer et sauvegarder la distribution multi-gaussienne des données de la première tâche**

----
##  3 - GUIDE DES CLASSES
----
L'écriture des DOCSTRINGS est en cours...
### **3.1 - Gestion des données et datasets (Datasets, DaS)**

La gestion des données est centralisée dans le fichier Datas.py. Celui-ci contient les classes *Datasets* et *DaS* ("Data Service"). La première charge les datasets à partir des chemins définis dans le fichier Configuration.py. La deuxième a une structure de Singleton et simule le flux de données incrémentales pendant l'apprentissage incrémental.

**Datasets :**

Soit ```d = Datasets()```. Cette ligne charge les datasets. Si une erreur survient, vérifiez l'exactitude des chemins. 

Les datasets sont les suivants : 

* mTrain : Base d'entraînement de la deuxième tâche (MNIST - 60.000 données) 
* tTrain : Base d'entraînement de la première tâche (chiffres typographiques 61.440 données*) 
* mTestConso : Base de test tâche 2 du modèle (MNIST - 8.004 données)
* tTestConso : Base de test tâche 1 du modèle (MNIST - 12.290 données*) 
* mValConso : Base de validation tâche 2 du modèle (MNIST - 1996 données) 
* tValConso : Base de validation tâche 1 du modèle (chiffres typo - 3070 données*) 

*Classes équilibrées (MNIST quasi équilibrée seulement ..)

Les getters associés renvoient le tuple ```(data, labels)```. Ainsi pour récupérer les données et labels de la base mTrain par exemple, utiliser la ligne suivante :

    (mTrainX, mTrainY) = d.getMTrain()

### **3.2 - Structure de la forêt (CustomRandomForestClassifier, CustomTree, MyTree)**

La structure de cette forêt a été très largement inscpirée de celle de scikit-learn qui néanmoins ne permettait pas de modifier modifier le contenu des noeuds des arbres (champs privés).

* ## Custom_RF

La classe *CustomRandomForestClassifier* définit la structure de notre forêt incrémentale. Celle-ci hérite directement de la classe *ForestClassifier* qui est une classe privée du package *scikit-learn.ensemble* et qui a donc été créer à l'identique dans le fichier ScikitLearnForest.py.

La forêt *CustomRandomForestClassifier* est identique à la forêt *RandomForestClassifier* de scikit-learn à ceci près que l'estimateur de base est maintenant l'objet *CustomTree* et nom *DecisionTreeClassifier*.

Il est donc possible d'utiliser cette forêt avec les mêmes méthodes que vous avez l'habitude d'utiliser pour la forêt de scikit-learn.

* ## CustomDecisionTreeClassifier

Là encore cette classe est directement inspirée de l'objet *DecisionTreeClassifier* de scikit-learn. Toutefois elle contient maintenant 3 nouveaux attributs :
- ```leaves_increment```, qui est un dictionnaire où chaque feuille (clé), atteinte par l'une des stratégie ULS/IGT, contient les valeurs des données incrémentales passées (*in-fine* cet attribut ne devrait stocker que des indices, les données elles-mêmes seraient stockées en un seul endroit).
- ```value_increment```, qui est un tableau de taille (nodes + leaves) * nb_classes et contient en fait les effectifs des données incrémentales passées par ces noeuds/feuilles pour chaque classe.

> Note : Dans les faits, les noeuds internes ne voient pas leurs effectifs modifiés, il serait donc possible de modifier cet objet en dictionnaire et de faire "remonter" les effectifs des feuilles dans le noeuds qui nous intéresse si une stratégie "destructrice" telle qu'RTST est mise en oeuvre.

- ```values```, contient les effectifs des données de la première tâche d'entraînement pour chaque classe. Ces effectifs étaient à l'origine stockés dans les feuilles directement (dans l'objet tree_) mais par souci de maîitrise de cython cette responsabilité a été donnée à l'objet *CustomTree*.

> L'ancien attribut ```tree_``` a été retiré et remplacé par l'attribut ```tree``` qui est maintenant du type *MyTree* qui est inspiré de la classe *Tree* (scikit-learn).

* ## mytree.pyx

La classe *MyTree* est directement inspirée du code de scikit-learn avec quelques modifications pour permettre l'ajout de setters aux attributs notamment.

* ## Manipuler l'objet CustomTree
Soit ```decisionTree = CustomTree(max_depth = 22, max_features = 16)``` entraîné une première fois avec la fonction *fit* sur les données d'entraînement *tTrain*.

```decisionTree.tree.node_count``` est le nombre de noeuds+feuilles de l'arbre

```decisionTree.tree.n_leaves``` est le nombre de feuilles de l'arbre

```decisionTree.tree.max_depth``` est la profondeur maximale de l'arbre

```decisionTree.tree.depth[i]``` est la profondeur du noeud ou de la feuille n°i

```decisionTree.tree.children_left[i]``` est le numéro du noeud/feuille enfant gauche du noeud n°i ( = -1 si le noeud n°i est une feuille)

```decisionTree.tree.children_right[i]``` est le numéro du noeud/feuille enfant du noeud droit n°i ( = -1 si le noeud n°i est une feuille)

```decisionTree.tree.feature[i]``` est le seuil du noeud n°i ( = -2 si le noeud n°i est une feuille)

```decisionTree.tree.impurity[i]``` est l'impureté dans le noeud n°i (gini par défaut)


> Attention : Évitez d'ajouter des tableaux (pointeurs) comme attributs à la classe MyTree si vous n'êtes pas sûr de ce que vous faites ! Là où Python possède sa propre gestion de la mémoire, le C nécessite de le faire manuellement.



### **3.3 - Gérer les constantes (Configuration)**

Le fichier Configuration.py contient les différentes constantes des entraînements 1 et 2.
Pour transmettre les valeurs des paramètres N, K, F, C et la fonction de poids aux  mémoires LTM et STM pendant l'entraînement de la tâche 2, le singleton *Config* est utilisé.
**Il est nécessaire d'instancier cet objet avant les mémoires au début de l'apprentissage de la 2e tâche !**

### **3.4 - Les résultats d'un apprentissage incrémental (GraphicResult)**

Dans le fichier GraphicReport.py vous trouverez les classes utiles à la récolte et la sauvegarde des résultats de l'appretnissage incrémental. Ces résutats sont les suivants :

- ```depths``` : la profondeur maximale des arbres de la STM à chaque perfectionnement (*Boxplot = tuple(Min, Q1, median, Q3, Max))
- ```leaves``` : le nombre de feuilles des arbres de la STM à chaque perfectionnement (*Boxplot)
- ```confusion_matrix``` : les matrices de confusion à chaque évaluation de l'échantillon entrant par la STM --> ```confusion_matrix[0]``` désigne la matrice de confusion de la toute première évaluation de l'apprentissage
- ```mc_manu_indices``` (expérimental => pas de shuffle des données) : contient la liste des indices des données incrémentales mal classées qui ont donc servi à la modification des arbres. Les données correspondantes sont donc ```mTrainX[mc_manu_indices]```.
- ```consolidations_typo``` : contient le taux de reconnaissance par classe de la LTM à chaque consolidation sur les données de test typographiques. 
- ```consolidations_manu``` : contient le taux de reconnaissance par classe de la LTM à chaque consolidation sur les données de test manuscrites.
> Attention ! La base ```mTestConso``` n'est pas équilibrée ! Par conséquent, pour obtenir une performance moyenne typo+manu, il faut réaliser une moyenne pondérée par les effectifs des classes de la base ```mTestConso``` (attribut ```manu_test_data_counts```) --> utiliser :
>```perf_manu = np.average(report.consolidations_manu, axis = 1, weights = report.manu_test_data_counts/sum(report.manu_test_data_counts) )```

- ```igts_applied``` : contient le nombre d'IGT appliqués par arbre pour chaque perfectionnement (*Boxplot)
- ```size_of_forest_sbs``` : contient la taille de la forêt LTM (avec sbs) à chaque consolidation
- ```sbs_selected_estimators``` : contient la liste des indices des arbres de la forêt LTM(sans sbs) de la version sbs-isée de la forêt

> Voir partie 4.7 pour exporter les résultats au format .xlsx

### **3.5 - La mémoire à court terme (STM)**

### **3.6 - Les stratégies ULS et IGT**

### **3.7 - La mémoire à long terme (LTM)**
----

##  4 - GUIDE D'UTILSIATION
----
### **4.1 - Importer un dataset local**
Instancier la classe *Datasets* pour importer tous les datasets :

    In [1]: d = Datasets()
            Loading typo dataset...
            Loading Manu dataset...
            Initializing mVal...
Récupérer les datasets qui vous intéressent à partir des getters associés :

    In [2]: (tTrainX, tTrainY) = d.getTTrain()

    In [3]: (mTestConsoX, mTestConsoY) = d.getMTestConso()

### **4.2 - Entraîner une forêt pour l'apprentissage de la première tâche**
Instancier la classe *CustomRandomForestClassifier* :

    In [4]: rf = CustomRandomForestClassifier(n_estimators = 100, max_depth = 22)

Entraîner le modèle sur la première tâche; Ici la distinction des chiffres typographiques. Cela prend moins d'une minute pour *tTrain* contenant 61440 chiffres : 

    In [5]: rf.fit(tTrainX, tTrainY)
    Out[5]: 
    CustomRandomForestClassifier(bootstrap=True, max_depth=22, max_features='auto',
                                max_leaf_nodes=None, max_samples=0.9,
                                min_impurity_decrease=0.0, min_impurity_split=None,
                                min_samples_leaf=1, min_samples_split=2,
                                min_weight_fraction_leaf=0.0, n_estimators=100,
                                n_jobs=4, oob_score=False, random_state=None,
                                verbose=0)


### **4.3 - Tester les performances d'une forêt sur un dataset**
Il suffit ici d'importer le dataset sur lequel la forêt sera évaluée puis d'utiliser la fonction *score* héritée de la forêt de scikit learn :

    In [6]: (tValConsoX, tValConsoY) = d.getTValConso()

    In [7]: rf.score(tValConsoX,tValConsoY)
    Out[7]: 0.9859934853420196

Dans cet exemple notre forêt a donc un score de 98.60% en validation. 
Le résultat étant aléatoire il lest préférable de relancer un entraînement plusieurs fois (boucle for) et de conserver la forêt entraînée qui a les meilleures performances sur la base de validation. Cette base de validation ici est *tValConso*. Utiliser la fonction multi_training dans le fichier Socle.py :

    In [8]: best_RF = multi_training(tTrainX, tTrainY, tValConsoX, tValConsoY, nb_estimators = 100, max_depth = 22)

### **4.4 - Sauvegarder une forêt comme forêt initiale de l'apprentissage incrémental**

Dans ce même fichier Socle.py, utiliser la fonction *saveRF* (penser à modifier le nom du fichier de sortie si besoin):

    In [9]: saveRF(best_RF)


### **4.5 - Configurer un apprentissage incrémental**

Format des images :

    im_height, im_width, im_channel = 28, 28, 1
Nombre de classes à prédire :

    n_classes = 10
Nombre de features en sortie du CNN :

    n_features = 256

Chemin vers la forêt initiale :

    RF_filename = './RandomForest.sav'
Chemin vers la distribution de données typographiques : 

    distrib_filename = './global_distrib_mean_var.sav'

Paramètres de la fonction de poids :

    alpha = 1000
    beta = 2000

Pondération des performances typo/manu dans le calcul des performances moyennes en validation lors de l'application de SBS et de la comparaison LTM / best_STM :

    weight_t = 0.5
    weight_m = 0.5

Les paramètres N, K, F et C sont initialisés lors de l'instanciation du singleton *Configuration*.

### **4.6 - Lancer un apprentissage incrémental et comprendre les sorties de la console**

Pour réaliser un GridSearch utiliser plutôt le fichier Main.py et configurer le grid search dans le fichier Configuration.py. Pour lancer un simple apprentissage incrémental utiliser le fichier Launcher.py en pensant à modifier les valeurs initiales des paramètres.

    ##Launcher.py

    N, K, F, n_samples = 100, 5, 0.3, 60000
    conso = math.ceil(n_samples/(K*N))

```n_samples``` correspond au nombre de données incrémentales que vous souhaitez utiliser pour l'entraînement incrémental. Le paramètre ```C``` est ainsi déduit avec les paramètres ```n_samples```, ```N``` et ```K```.

Pour choisir le format du fichier de rapport d'entraînement, modifier cette ligne :

    report_filename = "report_conso_" + str(conso) + "_f_" + str(F) + "_k_" + str(K) + "_n_" + str(N)

Il est maintenant temps de lancer un entraînement ! Pour cela compilez simplement le fichier Launcher.py. Dans la console doivent apparaître les lignes suivantes :

    ###########################################

    __main__ : Initialize configurations
            N=100, K=1, C=1, W_init=10.000000


    __main__ : Initialize all services
    Data __init__: Load datasets
            Loading typo dataset : OK
            Loading Manu dataset : OK
            Initializing mVal : OK
    LTM __init__
            Explicite __init__: OK
            Calculating initial performances ...

Et après quelques instants :

            Implicite __init__: OK
    STM __init__: ...
            EmpanMnesique __init__: OK
            MDT __init__: OK


    ###########################################
    __main__ : START incremental learning

Ces lignes montrent l'initialisation des différents singletons du programme à partir de la configuration définie en amont. 

Le début d'un cycle de consolidation est marqué par :

    __main__ : START consolidation 1

Dans un cycle de consolidation on trouve ```K``` cycles de perfectionnements. Un cycle s'écrit :

    STM __learning__: k = 1
            STM __learning__: total wrong predictions 24/100
            Improvement on mVal (size=100) for 2 STMs
                    Best STM index is 1 with score 0.99

Ici on lit que la première STM n'est pas parvenue à classer correctement 24 chiffres sur les 100 (=N) présents dans l'échantillon. Les opérations de modification s'opèrent juste après l'affichage de cette deuxième ligne. Puis la meilleure STM est choisie parmi toutes celles des perfectionnements passés. Dans l'exemple il y en a 2 qui sont testées sur mVal qui est de taille 100 en ce début d'apprentissage. 

> Note : L'objectif final étant de pouvoir couper le programme et faire des apprentissages successifs avec des pauses entre chacun, mVal sera sauvegardée pour reprendre l'apprentissage là où il s'était arrêté.

Ici la meilleure STM est celle qui a été modifiée (index = 1), elle obtient un score de 99% sur mVal.

Après ```K``` perfectionnements le cycle de consolidation s'achève avec le choix de la meilleure forêt LTM "sbs-isée". Entre autres, y sont décrits les résultats en validation pour les chiffres manuscrits et chiffres typographiques puis les résultats en test.

    LTM __consolidate__: calculating scores for comparison
    LTM __consolidate__: applying sbs
            Typo cVal : size= 3070
                SBS(Best STM) score = 0.9872964169381108
                Implicite RF score = 0.98762214
            Manu cVal: size= 1996
                SBS(Best STM) score = 0.8296593186372746
                Implicite RF score = 0.77104208
            LTM __consolidate__: Implicite_LTM's RF upgraded
                    Typo cTest : size= 12290
                        Implicite RF score = 0.9899104963384866
                    Manu cTest: size= 8004
                        Implicite RF score = 0.8209645177411296
    __main__ : END consolidation TIME1379.2816274166107

En particulier ici les forêts LTM et STM sbs-isées avaient respectivement les scores 98.76% et 98.73% en typpographique et 77.10% et 82.97% en manuscrit. La STM a donc remplacé la LTM courante.
**Les performances finales (en test) de la forêt LTM sbs-isée en fin de consolidation sont donc 98.99% en typographique pour 82.10% en manuscrit**.


### **4.7 - Récupérer les résultats d'un apprentissage incrémental au format Excel**

A la fin de l'entraînement incrémental l'objet ```reps.current_report``` (instance d'objet *Report*) contient l'ensemble des résultats récoltés au cours de l'apprentissage. Celui-ci est sauvegardé en tant qu'objet Python dans le fichier ```{report_filename}.pickle"```. Pour convertir cet objet en fichier excel (si cela n'est pas déjà fait dans le script actuel) utiliser la fonction dédiée dans le fichier GraphicReport.py.

    In[11]: fromReportToExcel( reps.current_report, out_filename )

Où ```out_filename``` désigne le nom du fichier excel a créer (contenant l'extension .xlsx).