# -*- coding: utf-8 -*-
"""
Created on Sun Dec  8 01:31:06 2019

@author: bourd
"""
from Datas import DaS
from LTM import LTM
from STM import STM
from Configuration import Config, RF_filename, GridSearch
import pickle, time, os
from Custom_RF import CustomRandomForestClassifier
from GraphicReport import Report, ReportsUtil, fromReportToExcel
import argparse


"""
    Fonction permettant de charger la forêt initiale et 
    d'initialiser les singletons DaS et LTM ainsi que la classe STM
"""
def init(N, K, F, C):
    print("###########################################\n")
    print("__main__ : Initialize configurations")
    Config(N, K, F, C)
    
    with open(RF_filename, 'rb') as f:
        print("Loading forest...")
        forest = pickle.load(f)
        forest.n_jobs = 4 #cpu_count
        forest.verbose = 0
    
    ## init all singleton
    print("\n\n__main__ : Initialize all services")
    DaS(force_init = True); LTM(forest, force_init = True); stm = STM(forest)
    
    return forest, stm


"""
    Sauvegarde des résultats et de la forêt ltm
"""
def save(rep, out_dir):
    output_dir = "training_result/" + out_dir + "/f_" + str(rep.F) + "_k_" + str(rep.K) + "_n_" + str(rep.N) + "_c_" + str(rep.C)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    
    print("\n__main__ : Saving report pickle file in " + output_dir)
    with open(output_dir + "/report.pickle","wb") as file_:
        pickle.dump(rep, file_, pickle.HIGHEST_PROTOCOL)
    print("\n__main__ : Saving report as xlsx file in " + output_dir)
    fromReportToExcel(rep, output_dir + "/report.xlsx")

    
"""
    Lancement de C cycles d'apprentissage incrémental (K perfectionnements + 1 consolidation)
"""
def cycle(stm, forest, out_dir, is_grid_search_cycle = False, save_at_each_conso = False):
    
    if is_grid_search_cycle :
        print("\n\n__main__ : init singletons for new experiment")
        DaS(force_init = True)
        LTM().reset(forest)
        stm = STM(forest)
        
    config = Config()
    report = Report(LTM(), config.K, config.N, config.C, config.F)
    ReportsUtil().setCurrentReport(report)
        
    print("\n\n###########################################")
    print("__main__ : START incremental learning")
    for c in range(1,config.C+1):
        print("\033[1;31;47m__main__ : cycle " + str(c) + "\033[0m")
        start = time.time()
        stm.launchOneIncrementalLearningCycle()
        end = time.time()
        print("\033[1;31;47m__main__ : TIME " + str(end - start) + "\033[0m")
        
        if save_at_each_conso:
            ReportsUtil().current_report.forest = LTM().getImpliciteRF()
            save(ReportsUtil().current_report, out_dir)
    print("\n__main__ : END incremental learning")
    if not save_at_each_conso:
        ReportsUtil().current_report.forest = LTM().getImpliciteRF()
    
    return stm

  
def run(output_dir, N, K, F, C, grid_search = False, save_at_each_conso = False):
    if not os.path.exists("training_result/" + output_dir):
        os.makedirs("training_result/" + output_dir)
    
    forest, stm = init(N, K, F, C)
    
    ## grid search
    if grid_search:
        for f in GridSearch['F']:
            for (k,n,c) in GridSearch['K_N_C'].values():
                Config(n,k,f,c)
                stm = cycle(stm, forest, output_dir, True, save_at_each_conso)
    ## apprentissage incrémental simple
    else:
        cycle(stm, forest, output_dir, False, save_at_each_conso)
        
    
    if not save_at_each_conso:
        ReportsUtil().setCurrentReport(None)
        for rep in ReportsUtil().reports:
            save(rep, output_dir)
    


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('output_directory', type=str, help="Output directory where to save results (does not need to exist)")
    parser.add_argument("-g", "--grid-search", action="store_true", help="Run Grid Search", dest = 'g')
    parser.add_argument("-s", "--save-conso", action="store_true", dest = 's', help="Save ltm and report at the end of each consolidation. Warning ! It increases computation time")
    parser.add_argument("-N", help="Incremental batch size", type=int, default=100)
    parser.add_argument("-K", help="Improvements number", type=int, default=10)
    parser.add_argument("-F", help="Impurity threshold", type=float, default=0.3)
    parser.add_argument("-C", help="Consolidations number", type=int, default=1)
    args = parser.parse_args()
    
    N = args.N; K = args.K; F = args.F; C = args.C; grid_search = args.g
    save_at_each_conso = args.s
    output_dir = args.output_directory
    
    run(output_dir, N, K, F, C, grid_search, save_at_each_conso)
    









