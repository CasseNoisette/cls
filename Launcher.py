# -*- coding: utf-8 -*-
"""
Created on Tue Jan 21 14:11:55 2020

@author: bourd
"""

from Datas import DaS
from LTM import LTM
from STM import STM
from Configuration import Config, RF_filename
import pickle
from Timer import Timer
import os
import shutil
import math
from datetime import datetime
from GraphicReport import Report, ReportsUtil, fromReportToExcel
from Custom_RF import CustomRandomForestClassifier



# Parameters initialization choice
N, K, F, n_samples = 100, 1, 0.8, 100
conso = math.ceil(n_samples/(K*N))

report_filename = "report_conso_" + str(conso) + "_f_" + str(F) + "_k_" + str(K) + "_n_" + str(N)

directory = './' + report_filename + '___' + datetime.now().strftime("%c").replace(':','_')
os.makedirs(directory)

print("###########################################\n")
print("__main__ : Initialize configurations")

conf = Config(N , K, F, conso)

with open(RF_filename, 'rb') as f_:
    #forest = CustomRandomForestClassifier(100,22)
    forest = pickle.load(f_)
    forest.n_jobs = 4
    forest.verbose = 0



if os.path.exists('./mVal_dataset/') :
    shutil.rmtree('./mVal_dataset/')


## init all singleton
print("\n\n__main__ : Initialize all services")
das = DaS(force_init = True)
ltm = LTM(forest, force_init = True)
stm = STM(forest)


reps = ReportsUtil() # will contain several results about incremental learning
reps.current_report = Report(ltm, K, N, conso, F)
print("\n\n###########################################")
print("__main__ : START incremental learning")
Timer() # start timer
for c in range(1,Config().C+1):
    print("\033[1;31;47m__main__ : START consolidation " + str(c) + "\033[0m")
    stm.launchOneIncrementalLearningCycle()
    print("\033[1;31;47m__main__ : END consolidation TIME " + str(round(Timer().getTime())) + "\033[0m")
print("\n__main__ : END incremental learning")


### save results and final forest
with open(directory + '/' + report_filename + ".pickle","wb") as file_:
    pickle.dump(reps.current_report, file_, pickle.HIGHEST_PROTOCOL)

fromReportToExcel(reps.current_report, directory + '/' + report_filename + ".xlsx")

forest_ = ltm.getImpliciteRF()
with open(directory + '/' + report_filename + "__forest_only.pickle","wb") as file_:
    pickle.dump(forest_, file_, pickle.HIGHEST_PROTOCOL)

