# -*- coding: utf-8 -*-
"""
Created on Sun Dec  8 00:47:40 2019

@author: bourd
"""
import numpy as np, json, os
#import pyper as pr
#import time
#
#r=pr.R(use_pandas=True)
#t = r("library(tmvtnorm)")
#t = r("test = 0")
#print("Loading R extension ...")
#while r.get("test") == None:
#    time.sleep(0.5)
#    t = r("test = 0")

if os.getcwd().split('\\')[-1] != "cls":
    raise Exception("Working directory should be cls")

with open("configuration.json", "rb") as file_:
    data = json.load(file_)

""" 1) data information """
n_classes = data["input_data"]["nb_classes"]
n_features = data["input_data"]["nb_features"]
im_height, im_width, im_channel = data["input_data"]["img_height"], data["input_data"]["img_width"], data["input_data"]["img_channels"]

tTrain_path = data["data_paths"]["training"]["typo_data"]
ltTrain_path = data["data_paths"]["training"]["typo_label"]

mTrain_path = data["data_paths"]["training"]["manu_data"]
lmTrain_path = data["data_paths"]["training"]["manu_label"]

tVal_conso_path = data["data_paths"]["consolidation_validation"]["typo_data"]
ltVal_conso_path = data["data_paths"]["consolidation_validation"]["typo_label"]

mVal_conso_path = data["data_paths"]["consolidation_validation"]["manu_data"]
lmVal_conso_path = data["data_paths"]["consolidation_validation"]["manu_label"]

tTest_conso_path = data["data_paths"]["consolidation_test"]["typo_data"]
ltTest_conso_path = data["data_paths"]["consolidation_test"]["typo_label"]

mTest_conso_path = data["data_paths"]["consolidation_test"]["manu_data"]
lmTest_conso_path = data["data_paths"]["consolidation_test"]["manu_label"]


mVal_path = data["data_paths"]["improvement_validation"]

cnn_acp_file = data["models_path"]["cnn_acp"]

""" 2) Incremental learning default params """
GridSearch = { 'F': data["grid_search"]["F"], 
              'K_N_C':data["grid_search"]["triplet_K_N_C"]}


""" 3) LTM params """
RF_filename = data["models_path"]["initial_forest"]
distrib_filename = data["models_path"]["training_data_distribution"]

weight_t = data["learning_weight_typo_manu"]["typo"]
weight_m = data["learning_weight_typo_manu"]["manu"]

""" 4) Gaussian Models construction """
step = 0.05
colors = ['black','gray','rosybrown','red','sienna','gold','chartreuse','darkgreen','deepskyblue','mediumvioletred']

W_init = data["decreasing_weight_model"]["w_init"]
alpha = data["decreasing_weight_model"]["alpha"]
beta = data["decreasing_weight_model"]["beta"]

class Config:

    instance = None

    class __Config:

        def __init__(self, n, k, f, c):
            if n is None:
                return
            self.N = int(n)
            self.K = int(k)
            self.C = int(c)
            self.F = f
            self.W_init = W_init

            w_courbeA = np.linspace(W_init,max(W_init - 1, 1),alpha)
            w_courbeB = np.linspace(max(W_init - 1, 1),1,beta)
            w_courbeC = np.array([1]*1000)

            self.w_function = np.concatenate((w_courbeA, w_courbeB, w_courbeC))
            print("\tN=%d, K=%d, C=%d, W_init=%f"%(n, k, c, W_init))


    def __new__(self, n = None, k = None, f = None, c = None, W_init = 10.0):
        if not Config.instance:
            Config.instance = Config.__Config(n, k, f, c)
        return Config.instance
