# -*- coding: utf-8 -*-
"""
Created on Sun Feb  9 22:23:53 2020

@author: bourdoulous

Inspiré du code scikit-learn tree_
"""

from sklearn.tree._tree cimport Tree
from sklearn.tree._splitter cimport Splitter
from sklearn.tree._utils cimport sizet_ptr_to_ndarray
from libc.stdlib cimport free
from libc.math cimport fabs
from libc.string cimport memcpy
from libc.string cimport memset
from libc.stdint cimport SIZE_MAX
from numpy import float32 as DTYPE
from numpy import float64 as DOUBLE
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
from cpython cimport Py_INCREF, PyObject, PyTypeObject, array
import numpy as np
cimport numpy as np
np.import_array()
from scipy.sparse import csr_matrix

ctypedef np.npy_float32 DTYPE_t          # Type of X
ctypedef np.npy_float64 DOUBLE_t         # Type of y, sample_weight
ctypedef np.npy_intp SIZE_t              # Type for indices and counters
ctypedef np.npy_int32 INT32_t            # Signed 32 bit integer
ctypedef np.npy_uint32 UINT32_t          # Unsigned 32 bit integer

cdef public SIZE_t TREE_LEAF = -1
cdef public SIZE_t TREE_UNDEFINED = -2

cdef public SIZE_t CHILDREN_LEFT = 0
cdef public SIZE_t CHILDREN_RIGHT = 1
cdef public SIZE_t FEATURE = 2
cdef public SIZE_t THRESHOLD = 3
cdef public SIZE_t IMPURITY = 4
cdef public SIZE_t DEPTH = 5


cdef struct Node:
    # Base storage structure for the _nodes in a Tree object

    SIZE_t left_child                    # id of the left child of the node
    SIZE_t right_child                   # id of the right child of the node
    SIZE_t feature                       # Feature used for splitting the node
    DOUBLE_t threshold                   # Threshold value at the node
    DOUBLE_t impurity                    # Impurity of the node (i.e., the value of the criterion)
    SIZE_t depth



cdef class MyTree:

    cdef public SIZE_t n_features
    cdef public SIZE_t node_count

    cdef Node* _nodes

    property nodes:
        def __get__(self):
            return self._get_node_ndarray()

        def __set__(self, values):
            self.node_count = len(values)
            if self._nodes != NULL:
                PyMem_Free(self._nodes)
            self._nodes = <Node*> PyMem_Malloc(sizeof(Node) * self.node_count)
            if not self._nodes:
                raise MemoryError()
            for i in range(self.node_count):
                self._nodes[i].left_child = <SIZE_t> values[i,CHILDREN_LEFT]
                self._nodes[i].right_child = <SIZE_t> values[i,CHILDREN_RIGHT]
                self._nodes[i].feature = <SIZE_t> values[i,FEATURE]
                self._nodes[i].threshold = <DOUBLE_t> values[i,THRESHOLD]
                self._nodes[i].impurity = <DOUBLE_t> values[i,IMPURITY]
                self._nodes[i].depth = <SIZE_t> values[i,DEPTH]

    property max_depth:
        def __get__(self):
            return int(np.max(self.nodes[:self.node_count, DEPTH]))

    property depth:
        def __get__(self):
            return self.nodes[:self.node_count, DEPTH]

    property children_left:
        def __get__(self):
            return self.nodes[:self.node_count, CHILDREN_LEFT]

    property children_right:
        def __get__(self):
            return self.nodes[:self.node_count, CHILDREN_RIGHT]

    property n_leaves:
        def __get__(self):
            return np.sum(np.logical_and(
                self.children_left == -1,
                self.children_right == -1))

    property feature:
        def __get__(self):
            return self.nodes[:self.node_count, FEATURE]

    property threshold:
        def __get__(self):
            return self.nodes[:self.node_count, THRESHOLD]

    property impurity:
        def __get__(self):
            return self.nodes[:self.node_count, IMPURITY]


    def __init__(self, node_count, n_features):
        self.node_count = node_count
        self.n_features = n_features
        self._nodes = NULL


    def setNodes(self,np.ndarray thresholds, np.ndarray impurities, np.ndarray features,
                  np.ndarray children_left, np.ndarray children_right):

        self._nodes = <Node*> PyMem_Malloc(self.node_count * sizeof(Node))

        self._nodes[0].left_child = <SIZE_t> children_left[0]
        self._nodes[0].right_child = <SIZE_t> children_right[0]
        self._nodes[0].feature = <SIZE_t> features[0]
        self._nodes[0].threshold = <DOUBLE_t> thresholds[0]
        self._nodes[0].impurity = <DOUBLE_t> children_right[0]
        self._nodes[0].depth = 0
        self._nodes[ self._nodes[0].left_child ].depth = 1
        self._nodes[ self._nodes[0].right_child ].depth = 1
        for i in range(1, self.node_count):
            self._nodes[i].left_child = <SIZE_t> children_left[i]
            self._nodes[i].right_child = <SIZE_t> children_right[i]
            self._nodes[i].feature = <SIZE_t> features[i]
            self._nodes[i].threshold = <DOUBLE_t> thresholds[i]
            self._nodes[i].impurity = <DOUBLE_t> impurities[i]
            if self._nodes[i].left_child != TREE_LEAF:
                self._nodes[ self._nodes[i].left_child ].depth = self._nodes[i].depth + 1
                self._nodes[ self._nodes[i].right_child ].depth = self._nodes[i].depth + 1

    cpdef np.ndarray apply(self, object X):
        # Check input
        if not isinstance(X, np.ndarray):
            raise ValueError("X should be in np.ndarray format, got %s"
                             % type(X))

        if X.dtype != DTYPE:
            raise ValueError("X.dtype should be np.float32, got %s" % X.dtype)

        # Extract input
        cdef DTYPE_t[:, :] X_ndarray = X
        cdef SIZE_t n_samples = X.shape[0]

        # Initialize output
        cdef np.ndarray[SIZE_t] out = np.zeros((n_samples,), dtype=np.intp)
        cdef SIZE_t* out_ptr = <SIZE_t*> out.data

        # Initialize auxiliary data-structure
        cdef Node* node = NULL
        cdef SIZE_t i = 0

        with nogil:
            for i in range(n_samples):
                node = self._nodes
                # While node not a leaf
                while node.left_child != TREE_LEAF:
                    # ... and node.right_child != _TREE_LEAF:
                    if X_ndarray[i, node.feature] <= node.threshold:
                        node = &self._nodes[node.left_child]
                    else:
                        node = &self._nodes[node.right_child]

                out_ptr[i] = <SIZE_t>(node - self._nodes)  # node offset

        return out


    def _get_node_ndarray(self):
        if self._nodes == NULL:
            return np.array([])
        return np.array([ [self._nodes[i].left_child, self._nodes[i].right_child, self._nodes[i].feature, \
                  self._nodes[i].threshold, self._nodes[i].impurity, self._nodes[i].depth] \
                    for i in range(self.node_count)])



    cpdef object decision_path(self, object X):
        if not isinstance(X, np.ndarray):
            raise ValueError("X should be in np.ndarray format, got %s"
                             % type(X))

        if X.dtype != DTYPE:
            raise ValueError("X.dtype should be np.float32, got %s" % X.dtype)

        # Extract input
        cdef DTYPE_t[:, :] X_ndarray = X
        cdef SIZE_t n_samples = X.shape[0]

        # Initialize output
        cdef np.ndarray[SIZE_t] indptr = np.zeros(n_samples + 1, dtype=np.intp)
        cdef SIZE_t* indptr_ptr = <SIZE_t*> indptr.data

        cdef np.ndarray[SIZE_t] indices = np.zeros(n_samples *
                                                   (1 + self.max_depth),
                                                   dtype=np.intp)
        cdef SIZE_t* indices_ptr = <SIZE_t*> indices.data

        # Initialize auxiliary data-structure
        cdef Node* node = NULL
        cdef SIZE_t i = 0

        with nogil:
            for i in range(n_samples):
                node = self._nodes
                indptr_ptr[i + 1] = indptr_ptr[i]

                # Add all external _nodes
                while node.left_child != TREE_LEAF:
                    # ... and node.right_child != _TREE_LEAF:
                    indices_ptr[indptr_ptr[i + 1]] = <SIZE_t>(node - self._nodes)
                    indptr_ptr[i + 1] += 1

                    if X_ndarray[i, node.feature] <= node.threshold:
                        node = &self._nodes[node.left_child]
                    else:
                        node = &self._nodes[node.right_child]

                # Add the leave node
                indices_ptr[indptr_ptr[i + 1]] = <SIZE_t>(node - self._nodes)
                indptr_ptr[i + 1] += 1

        indices = indices[:indptr[n_samples]]
        cdef np.ndarray[SIZE_t] data = np.ones(shape=len(indices),
                                               dtype=np.intp)
        out = csr_matrix((data, indices, indptr),
                         shape=(n_samples, self.node_count))

        return out

    cpdef void leaf_into_node(self, SIZE_t leaf, SIZE_t feature, DOUBLE_t threshold, DOUBLE_t impurity):
        self._nodes[leaf].feature = feature
        self._nodes[leaf].threshold = threshold
        self._nodes[leaf].impurity = impurity

    def __dealloc__(self):
        """Destructor."""
        # Free all inner structures
        PyMem_Free(self._nodes)

#    def __reduce__(self):
#        """Reduce re-implementation, for pickling."""
#        return (MyTree, (self.node_count, self.n_features), self.__getstate__())

    cdef bytes get_nodes_state(self):
        if self._nodes == NULL:
            return None
        else:
            return <bytes> (<char*> self._nodes)[:sizeof(Node) * self.node_count]


## construction de __getstate__ et __setstate__ inspirée de
## https://snorfalorpagus.net/blog/2016/04/16/pickling-cython-classes/
    def __getstate__(self):

        return (self.node_count, self.n_features, self.get_nodes_state())

    cdef void set_nodes_state(self, bytes nodes):
        self._nodes = <Node*> PyMem_Malloc(sizeof(Node) * self.node_count)

        if not self._nodes:
            raise MemoryError()
        memcpy(self._nodes, <char*> nodes, sizeof(Node) * self.node_count)

    def __setstate__(self, state):
        PyMem_Free(self._nodes)
        self.node_count = state[0]
        self.n_features = state[1]
        if state[2] == None:
            self._nodes = NULL
        else:
            self.set_nodes_state(state[2])


