# -*- coding: utf-8 -*-
"""
Created on Fri Feb 14 19:55:15 2020

@author: bourdoulous berangere

Ce script présente une approche pour la gestion de l'incrémentalité en classe à partir du code actuel
Ici on ajoute simplement 2 images représentant un "10" qui n'apparaissent pas dans l'entrainement initial

On peut réaliser cela éventuellement avec des symboles (utilisant des caractéristiques des chiffres => formes de baton ou circulaire)
en générant une grande quantité de ces symboles de la même façon que nous avons généré les données manu avec ImageMagick

Observer alors l'impact sur les perf d'un apprentissage incrémental avec ces symboles comme un incréments en classe

Les stratégies RTST et RUST, avec un léger oubli des typo en théorie, permet d'améliorer l'apprentissage des nouvelles classes


"""

import glob
import os
import numpy as np
from matplotlib.image import imread
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
import pickle
from copy import deepcopy
from Configuration import cnn_acp_file, im_height, im_width, im_channel, RF_filename
from CustomDecisionTreeClassifier import TreeUtils
from Datas import Datasets
from joblib import Parallel, delayed
from LTM import LoadTypoDistribution


expected_cwd = 'cls_algo\\cls'

def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.2989, 0.5870, 0.1140]) #  ITU-R 601-2 luma transform

if expected_cwd not in os.getcwd():
    raise RuntimeError("Current working directory should be " + expected_cwd)

def retrieveImages(directory_incremental_data, image_extension = "jpeg", show = False):
    files = glob.glob(directory_incremental_data + "/*." + image_extension)
    
    print(len(files), "images found")
    
    images = np.empty((0,28,28,1), dtype = np.float32)
    
    for filepath in files:
        image = imread(filepath)
        if im_channel == 1 and image.ndim == 3:
            image = rgb2gray(image)
        image = image.reshape(im_height, im_width, im_channel)
        if show:
            imshow(image)
            plt.show()
        ## normalize
        image /= 255
        images = np.append(images, [image], axis = 0)
    
    return images
    

directory_incremental_data = "../../cls_datasets/new_class_test"
image_extension = "jpeg"
classnumber = 10
## load forest and add class in memory
forest_path = RF_filename
with open(forest_path, "rb") as file_:
    forest = deepcopy(pickle.load(file_))
    for tree in forest.estimators_:
        tree.values = np.concatenate((tree.values, np.array([0] * len(tree.values)).reshape(-1,1)), axis = 1)
        tree.value_increment = np.concatenate((tree.value_increment, np.array([0] * len(tree.value_increment)).reshape(-1,1)), axis = 1)
        tree.n_classes += 1
    forest.classes_ = np.append(forest.classes_, classnumber)
    forest.n_classes_ += 1 


images = retrieveImages(directory_incremental_data)
with open(cnn_acp_file, "rb") as file_:
    cnn_acp_converter = pickle.load(file_)
X_images = cnn_acp_converter.getFeatures(images)

classes = np.array([classnumber] * len(X_images))

DATA = Datasets()
dist = LoadTypoDistribution()

init_score_typo = forest.score(DATA.getTTestConso()[0], DATA.getTTestConso()[1])
init_score_manu = forest.score(DATA.getMTestConso()[0], DATA.getMTestConso()[1])

predictions = forest.score(X_images, classes)

igts = Parallel(n_jobs=forest.n_jobs, verbose=forest.verbose,backend="threading")(
            delayed( TreeUtils.update_leaves)(it, tree, X_images, classes.reshape(-1,1), np.array([10] * forest.estimators_[0].n_classes), dist)
            for it, tree in enumerate(forest.estimators_))

predictions_after = forest.score(X_images, classes)

after_score_typo = forest.score(DATA.getTTestConso()[0], DATA.getTTestConso()[1])
after_score_manu = forest.score(DATA.getMTestConso()[0], DATA.getMTestConso()[1])


