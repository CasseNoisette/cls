# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 13:35:59 2019

@author: piotm
"""
import numpy as np
import pickle
import os
from Configuration import mVal_path, Config, n_features,\
                          mTrain_path, tTrain_path, lmTrain_path, ltTrain_path, \
                          tVal_conso_path, tTest_conso_path, ltVal_conso_path, \
                          ltTest_conso_path, mVal_conso_path, mTest_conso_path, \
                          lmVal_conso_path, lmTest_conso_path
import shutil


class Datasets :
    def __init__(self):


        with open(tTrain_path, "rb") as bin_f_data:
            with open(ltTrain_path, "rb") as bin_f_label:
                TrainX = pickle.load(bin_f_data)
                TrainY = pickle.load(bin_f_label)

        with open(tVal_conso_path, "rb") as bin_f_data:
            with open(ltVal_conso_path, "rb") as bin_f_label:
                ValX = pickle.load(bin_f_data)
                ValY = pickle.load(bin_f_label)

        with open(tTest_conso_path, "rb") as bin_f_data:
            with open(ltTest_conso_path, "rb") as bin_f_label:
                TestX = pickle.load(bin_f_data)
                TestY = pickle.load(bin_f_label)

        self.__tTrain = (np.array(TrainX), np.array(TrainY))
        self.__tValConso = (np.array(ValX), np.array(ValY))
        self.__tTestConso = (np.array(TestX), np.array(TestY))

        print('\tLoading typo dataset : OK')


        with open(mTrain_path, "rb") as bin_f_data:
            with open(lmTrain_path, "rb") as bin_f_label:
                TrainX = pickle.load(bin_f_data)
                TrainY = pickle.load(bin_f_label)

        with open(mVal_conso_path, "rb") as bin_f_data:
            with open(lmVal_conso_path, "rb") as bin_f_label:
                ValX = pickle.load(bin_f_data)
                ValY = pickle.load(bin_f_label)

        with open(mTest_conso_path, "rb") as bin_f_data:
            with open(lmTest_conso_path, "rb") as bin_f_label:
                TestX = pickle.load(bin_f_data)
                TestY = pickle.load(bin_f_label)

        self.__mTrain = (np.array(TrainX), np.array(TrainY))
        self.__mValConso = (np.array(ValX), np.array(ValY))
        self.__mTestConso = (np.array(TestX), np.array(TestY))

        print('\tLoading Manu dataset : OK')


        if os.path.exists('./mVal_dataset/'):
            shutil.rmtree('./mVal_dataset/')
            #self.__mVal = np.array(pickle.load(open(mVal_path, 'rb')), dtype = np.float32)
        os.makedirs('./mVal_dataset/')
        self.__mVal = None
        print('\tInitializing mVal : OK')

    def getMTrain(self):

        return self.__mTrain

    def getMTestConso(self):
        return self.__mTestConso

    def getMValConso(self):
        return self.__mValConso

    def getMVal(self):
        return self.__mVal

    def getTTrain(self):
        return self.__tTrain

    def getTValConso(self):
        return self.__tValConso

    def getTTestConso(self):
        return self.__tTestConso

    def setMVal(self, fifo):
        self.__mVal = fifo
        return self

    def saveMVal(self):
        with open(mVal_path, 'wb') as f:
            pickle.dump(self.__mVal, f)


class DaS(object):
    class __DaS:

        def __init__(self):
            print("Data __init__: Load datasets")
            self.__datasets = Datasets()
            self.__echantillon = None

            if self.__datasets.getMVal() != None :
                self.mVal = self.__datasets.getMVal()
            else :
                self.mVal = (np.empty((0, n_features), np.float64), np.empty((0, 1), np.uint8))

            self.__indice_max = self.__datasets.getMTrain()[0].shape[0]
            self.ech_it = 0


        def getEchantillon(self) :
            mTrain = self.__datasets.getMTrain()

            start = self.ech_it * Config().N
            end = (self.ech_it + 1) * Config().N

            if start > len(mTrain[0]):
                 e_indices = np.arange(start % len(mTrain), end % len(mTrain) )

            elif end > len(mTrain[0]) - 1:
                e_indices = np.concatenate((np.arange(start, len(mTrain) ),
                                           np.arange(0, end % len(mTrain) )))

            else:
                e_indices = np.arange(start, end)

            self.ech_it += 1
            ech_images_X = mTrain[0][e_indices].reshape(Config().N, n_features)
            ech_images_Y = mTrain[1][e_indices].reshape(Config().N,1)


            self.__echantillon = (ech_images_X, ech_images_Y)


            self.addEchantillonToMVal()

            return self.__echantillon

        def addEchantillonToMVal(self):
            if len(self.mVal[0]) == Config().K * Config().N : # si taille mVal atteint taille Max
                self.mVal = (np.delete(self.mVal[0], np.arange(Config().N), 0)
                            ,np.delete(self.mVal[1], np.arange(Config().N), 0))
            self.mVal = (np.append(self.mVal[0], self.__echantillon[0], axis=0)
                        ,np.append(self.mVal[1], self.__echantillon[1], axis=0))
            self.updateMVal()

        def getCVal(self):
            tVal = self.__datasets.getTValConso()
            mVal = self.__datasets.getMValConso()

            return (tVal, mVal)

        def getCTest(self):
            tTest = self.__datasets.getTTestConso()
            mTest = self.__datasets.getMTestConso()

            return (tTest, mTest)

        def getMVal(self):
            return self.__datasets.getMVal()

        def updateMVal(self):
            self.__datasets.setMVal(self.mVal)
            self.__datasets.saveMVal()


    instance = None
    def __new__(c, force_init = False):
        if force_init or not DaS.instance :
            DaS.instance = DaS.__DaS()
        return DaS.instance
