# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 21:23:04 2020

@author: bourdoulous berangere
"""

#### plot n=100,500,1500,3000
import pickle
import numpy as np
import matplotlib.pyplot as plt
import glob, argparse
from pathlib import Path
from matplotlib.lines import Line2D
from Configuration import n_classes

colors = ['green', 'red','gold','deepskyblue']
directory = ''
files = glob.glob(directory + "report*.pickle")

def DrawGlobalAccuracies(files, show = False):
    lines = []
    labels = []
    
    it = 0
    plt.figure(num=None, figsize=(15, 8), dpi=80, facecolor='w', edgecolor='k')
    
    max_samples = 0
    for filename in files:
        
        
        
        with open(filename,"rb") as file_:
            report = pickle.load(file_)
            
        perfs_manu = np.average(report.consolidations_manu, axis = 1, weights = report.classes_weights_typo )

        perfs_typo = np.average(report.consolidations_typo, axis = 1, weights = report.classes_weights_manu )

        plt.plot(np.arange(len(perfs_typo)) * report.K * report.N,
                 perfs_manu, marker='', color = colors[it], linewidth=2, linestyle='dashed')
        plt.plot(np.arange(len(perfs_typo)) * report.K * report.N,
                 perfs_typo, marker='', color = colors[it], linewidth=2)
        
    
        lines.append( Line2D([0], [0], color=colors[it], linewidth=3, linestyle='-') )
        labels.append( "N = " + str(report.N) + "; K = " + str(report.K) + "; F = " + str(report.F))
        
        if max_samples < report.C * report.K * report.N:
            max_samples = report.C * report.K * report.N
        
    plt.plot(np.arange(0,60001,10000) ,
                 [0.9736] * 7, marker='', color = 'black', linewidth=2, linestyle='dashed')
    
    lines.append( Line2D([0], [0], color="black", linewidth=3, linestyle='--') )
    labels.append( "Manuscrit")
    
    lines.append( Line2D([0], [0], color="black", linewidth=3, linestyle='-') )
    labels.append( "Typographique")
        
    plt.ylabel("Précision", fontsize=12)
    plt.xlabel("Nombre de données incrémentales reçues", fontsize=12)
    plt.xlim(0,max_samples)
    plt.title("Evolution du taux de reconnaissance de la LTM sur cTest \n pour " + str(len(files)) + " initialisation(s)", fontsize=16)
    plt.legend(lines, labels)
    plt.grid()
    if show:
        plt.show()

    
def DrawLeavesEvolution(files, show = False):
    lines = []
    labels = []
    
    it = 0
    plt.figure(num=None, figsize=(15, 8), dpi=80, facecolor='w', edgecolor='k')
    max_samples = 0
    
    for filename in files:
        
        with open(filename,"rb") as file_:
            report = pickle.load(file_)
          
        
        mc = np.array([np.sum(conf_m - np.diag(np.diag(conf_m))) for conf_m in report.confusion_matrix.reshape(report.kc, n_classes, n_classes)]).reshape(-1)
    
        if report. N == 100:
            w = 1
        else :
            w = 3
        plt.plot(np.arange(1,len(mc)+1) * report.N,
                 mc, marker='', color = colors[it], linewidth=w)
    
        
        if report. N == 100:
            w = 1
        else :
            w = 2
        plt.plot(np.arange(len(report.leaves)) * report.N,
                 report.leaves, marker='', color = colors[it], linewidth=w)
    
        lines.append( Line2D([0], [0], color=colors[it], linewidth=3, linestyle='-') )
        labels.append( "N = " + str(report.N) + "; K = " + str(report.K) + "; F = " + str(report.F))
        
        it += 1
        if max_samples < report.K * report.N * report.C:
            max_samples = report.K * report.N * report.C
        
    plt.ylabel("Nombre médian de feuilles par arbre", fontsize=12)
    plt.xlabel("Nombre de données incrémentales reçues", fontsize=12)
    plt.xlim(0,max_samples)
    plt.title("Evolution du nombre de feuilles par arbre \n pour " + str(len(files)) + " initialisation(s)", fontsize=16)
    plt.legend(lines, labels)
    plt.grid()
    if show:
        plt.show()
        
def run(directory, show = True):
    files = [path for path in Path(directory).rglob('report*.pickle')]
    
    DrawGlobalAccuracies(files, show)
    plt.savefig(directory + "/accuracies.svg")
    DrawLeavesEvolution(files, show)
    plt.savefig(directory + "/leaves.svg")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("directory", help="Directory containing reports or directories of reports")
    parser.add_argument("--show", action="store_true", help="Show figures")
    args = parser.parse_args()
    directory = args.directory
    
    run(directory, args.show)
    
        
