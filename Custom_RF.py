# -*- coding: utf-8 -*-
"""
Created on Tue Dec  3 2019.

@author: bourdoulous berangere

"""
from CustomDecisionTreeClassifier import CustomTree
from ScikitLearnForest import ForestClassifier
import numpy as np
from sklearn.metrics import accuracy_score
import copy

class CustomRandomForestClassifier(ForestClassifier):
    """Custom version of SKLEARN RandomForestClassifier with a Custom tree object as base_estimator."""

    def __init__(self,
                 n_estimators,
                 max_depth,
                 max_samples = 0.9,
                 min_samples_split=2,
                 min_samples_leaf=1,
                 min_weight_fraction_leaf=0.,
                 max_features="auto", #sqrt(n_features)
                 max_leaf_nodes=None,
                 min_impurity_decrease=0.,
                 min_impurity_split=None,
                 bootstrap=True,
                 oob_score=False,
                 n_jobs=4,
                 random_state=None,
                 verbose=0):
        super().__init__(
            base_estimator=CustomTree(max_depth = max_depth, max_features = max_features),
            n_estimators=n_estimators,
            estimator_params=("criterion", "max_depth", "min_samples_split",
                              "min_samples_leaf", "min_weight_fraction_leaf",
                              "max_features", "max_leaf_nodes",
                              "min_impurity_decrease", "min_impurity_split",
                              "random_state", "ccp_alpha"),
            bootstrap=bootstrap,
            oob_score=oob_score,
            n_jobs=n_jobs,
            random_state=random_state,
            verbose=verbose,
            warm_start=False,
            class_weight=None,
            max_samples=max_samples)

        self.criterion = "gini"
        self.max_depth = max_depth
        self.min_samples_split = min_samples_split
        self.min_samples_leaf = min_samples_leaf
        self.min_weight_fraction_leaf = min_weight_fraction_leaf
        self.max_features = max_features
        self.max_leaf_nodes = max_leaf_nodes
        self.min_impurity_decrease = min_impurity_decrease
        self.min_impurity_split = min_impurity_split
        self.ccp_alpha = 0.0

class ForestUtils:
    """Compute SBS method and get proba predictions of data of each tree."""

    @staticmethod
    def get_scores(predict_t, predict_m, expected_t, expected_m, subset_indices):
        """
        Compute global accuracy score on printed & handwritten validation data for a subset of trees.

        Parameters
        ----------
        predict_t: float[n_samples][n_features]
            predict_proba_distinct_trees method return statement with tValConso data
        predict_m: float[n_samples][n_features]
            predict_proba_distinct_trees method return statement with mValConso data
        expected_t: int[n_samples]
            tValConsoY
        expected_m: int[n_samples]
            mValConsoY
        subset_indices : int[<=n_trees]
            indices of trees to keep in calculation of global accuracy score

        Returns
        -------
        perfs_t : float
            Accuracy score of of predictions given by the "subset_indices" subset of trees for printed numbers.
        perfs_m : float
            Accuracy score of of predictions given by the "subset_indices" subset of trees for handwritten numbers.

        """
        perfs_m = accuracy_score(ForestUtils.compute_final_set_of_trees_decision(predict_m[subset_indices]), expected_m)
        perfs_t = accuracy_score(ForestUtils.compute_final_set_of_trees_decision(predict_t[subset_indices]), expected_t)

        return (perfs_t, perfs_m)

    @staticmethod
    def wrapperSBSmethod(forest, typo_X, typo_Y, manu_X, manu_Y, weight_t, weight_m):
        forest.estimators_ = np.array(forest.estimators_)
        predict_t = ForestUtils.predict_proba_distinct_trees(forest, typo_X)
        predict_m = ForestUtils.predict_proba_distinct_trees(forest, manu_X)

        forest.estimators_ = forest.estimators_[ForestUtils.SBSmethod(predict_t, predict_m, typo_Y, manu_Y, weight_t, weight_m)]

        return forest


    @staticmethod
    def SBSmethod(predict_t, predict_m, expected_t, expected_m, weight_t, weight_m):
        """Compute SBS method which finds the best subset of trees with better accuracy than with all trees.

        Parameters
        ----------
        predict_t: float[n_samples][n_features]
            predict_proba_distinct_trees method return statement with tValConso data
        predict_m: float[n_samples][n_features]
            predict_proba_distinct_trees method return statement with mValConso data
        expected_t: int[n_samples]
            tValConsoY
        expected_m: int[n_samples]
            mValConsoY
        weight_t: float < 1
            Weight given to printed numbers accuracy score in global accuracy_score in validation
        weight_m: float < 1
            Weight given to handwritten numbers accuracy score in global accuracy_score in validation

        Raises
        ------
        Exception
            if weight_t + weight_m != 1.

        Returns
        -------
        selected_estimators_idx: int[<=n_trees]
            Trees indices selected by SBS algorithm

        """
        #start = time.time()

        if weight_t + weight_m != 1:
            raise Exception("First task and second task weights sum must equal 1")

        ## init values
        selected_estimators_idx = np.arange(len(predict_t), dtype = np.uint8) ## contains all trees at the begining
        estimators_state = np.array([True] * len(predict_t)) # True at index i if tree i is selected else False

        # calculate init scores with all trees
        (best_perf_t, best_perf_m) = ForestUtils.get_scores(predict_t, predict_m, expected_t, expected_m, selected_estimators_idx)
        best_perf = (best_perf_m * weight_m + best_perf_t * weight_t) / (weight_t + weight_m)

        # remove estimators one by one while it increases accuracy score
        while(True):

            perfs = dict() # key : estimator removed ; value : score

            ## maybe this loop can be optimized?
            for estimator in selected_estimators_idx:
                estimators_state_copy = copy.copy(estimators_state)
                estimators_state_copy[estimator] = False

                (perfs_t, perfs_m) = ForestUtils.get_scores(predict_t,
                                                            predict_m,
                                                            expected_t,
                                                            expected_m,
                                                            np.argwhere(estimators_state_copy).reshape(-1))
                perfs[estimator] = (perfs_t * weight_t + perfs_m * weight_m) / (weight_t + weight_m)

            max_perf_estimator = max(perfs, key = perfs.get)

            # if True continue SBS
            if perfs[max_perf_estimator] > best_perf :

                best_perf = perfs[max_perf_estimator]

                estimators_state[max_perf_estimator] = False
                selected_estimators_idx = np.delete(selected_estimators_idx,
                                                        np.argwhere(selected_estimators_idx == max_perf_estimator)[0][0])

            # else return result
            else:
#               end = time.time()
#               print(str(end - start))
                return selected_estimators_idx

    @staticmethod
    def predict_proba_distinct_trees(forest, data):
        """Compute proba predictions of a dataset for each tree.

        Args
        ----
        forest: CustomRandomForestClassifier
            Forest's trees to use
        data: float[n_samples][n_features]

        Returns
        -------
        res: float[n_trees][n_samples][n_classes]
            res[i,j] contains prediction proba of each class for the j-th data sample by the i-th tree

        """
        return np.array([tree.predict_proba(data) for tree in forest.estimators_])

    @staticmethod
    def compute_final_set_of_trees_decision(predictions):
        """Compute final decision of a set of tree predict proba.

        Args
        ----
        predictions: float[n_trees][n_samples][n_classes]
            This is the return statement of predict_proba_distinct_trees method

        Returns
        -------
        proba: int[n_samples]
            Prediction of each sample.

        """
        all_proba = np.sum(predictions, axis = 0) / len(predictions) ## shape = (?, n_classes)

        pred = np.argmax(all_proba, axis = 1) ## shape = ?

        return pred

    @staticmethod
    def get_accuracy_score_by_class(forest, typo_X, typo_Y, manu_X, manu_Y):
        """
        Calculate accuracy score per class on datasets by a forest.

        Parameters
        ----------
        forest : CustomRandomForestClassifier
            Forest to use for prediction.
        typo_X : float[n_samples_t][n_features]
            Printed numbers validation data to predict.
        typo_Y : float[n_samples_t]
            Printed numbers validation labels to predict.
        manu_X : float[n_samples_m][n_features]
            Handwritten numbers validation data to predict.
        manu_Y : float[n_samples_m]
            Handwritten numbers validation labels to predict..

        Returns
        -------
        t_scores : float[n_classes]
            Accuracy_score on printed numbers validation dataset for each class.
        m_scores : float[n_classes]
            Accuracy_score on handwritten numbers validation dataset for each class.

        """
        predictionsT = forest.predict(typo_X)
        t_scores = np.array([ accuracy_score(typo_Y[typo_Y == c], predictionsT[typo_Y == c]) for c in range(forest.estimators_[0].n_classes)])

        predictionsM = forest.predict(manu_X)
        m_scores = np.array([ accuracy_score(manu_Y[manu_Y == c], predictionsM[manu_Y == c]) for c in range(forest.estimators_[0].n_classes)])

        return (t_scores, m_scores)