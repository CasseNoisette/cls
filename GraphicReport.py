# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 19:31:59 2020.

@author: bourdoulous berangere
"""
from Configuration import n_classes
import numpy as np
import copy
from pandas import DataFrame, ExcelWriter


from pympler import asizeof

def get_size(obj):
    """Calculate size of an object in memory.

    Args
    ----
        obj: any Python object.

    Returns
    -------
        Long type.

    """
    return asizeof.asized(obj).size/1000000

class Report :
    """Enable the storage of all the results produced during a learning experiment.

    Parameters
    ----------
    ltm: LTM
        LTM instance of the learning experiment
    K: int
        Number of improvements per consolidation of the learning experiment
    N: int
        Number of samples in an incremental set of the learning experiment
    C: int
        Total number of consolidation cycles of the learning experiment
    F: float
        Impurity threshold of the learning experiment

    Attributes
    ----------
    K: int
        Number of improvements per consolidation of the learning experiment
    N: int
        Number of samples in an incremental set of the learning experiment
    C: int
        Total number of consolidation cycles of the learning experiment
    F: float
        Impurity threshold of the learning experiment
    depths: tuple[K*C] with tuple of 5 integers
        Contains max-depth distribution Boxplot=(Q0, Q1, Q2, Q3, Q4) of trees in stm-forest for each improvement
    leaves: tuple[K*C] with tuple of 5 integers
        Contains number of leaves distribution Boxplot of trees in stm-forest for each improvement
    confusion_matrix: int[K*C][n_classes][n_classes]
        Contains confusion matrix of stm-forest evaluation at the begining of each improvement
    mc_manu_indices: int[?] EXPERIMENTAL
        Contains indices of misclassified samples at each stm-forest evaluation (indices of mTrain dataset)
    consolidations_typo: float[C][n_classes]
        Contains printed numbers accuracy score per class of LTM at the end of each consolidation cycle
    consolidations_manu: float[C][n_classes]
        Contains handwritten numbers accuracy score per class of LTM at the end of each consolidation cycle
    igts_applied: tuple[K*C] with tuple of 5 integers
        Contains the number of igts applied distribution Boxplot of trees in stm-forest for each improvement
    impurities: tuple[K*C] with tuple of 5 floats
        Contains impurity value after uls distribution Boxplot of trees in stm-forest for each improvement
    size_of_forest_sbs: long[C]
        Size in memory of ltm-forest WITH SBS at the end of each consolidation
    sbs_selected_estimators: int[?]
        Indices of selected trees for the latest ltm-forest WITH SBS
    typo_test_data_counts: int[n_classes]
        Counts of each class in tTestConso dataset (equal distribution)
    manu_test_data_counts: int[n_classes]
        Counts of each class in mTestConso dataset (not equal distribution)

    """

    def __init__(self, ltm, K, N, C, F):
        self.K = K
        self.C = C
        self.F = F
        self.N = N
        self.kc = 0
        self.c = 1
        self.depths = np.array([])
        self.leaves = np.array([])
        self.confusion_matrix = np.array([])
        self.mc_manu_indices = np.array([])
        self.consolidations_typo = np.zeros((C + 1, n_classes))
        self.consolidations_manu = np.zeros((C + 1, n_classes))

        self.consolidations_typo[0] = np.array([np.transpose(ltm.getTypoScore())])

        self.consolidations_manu[0] = np.array([np.transpose(ltm.getManuScore())])

        self.igts_applied = np.zeros((5, K * C))
        self.impurities = np.zeros((5, K * C))
        self.size_of_forest_sbs = np.array([get_size(ltm.getImpliciteRF())])
        self.classes_weights_typo = ltm.classes_weights_typo
        self.classes_weights_manu = ltm.classes_weights_manu


    def update_igts_and_impurities(self, igts, impurities):
        """Add igts results.

        Args
        ----
            igts: array of the number of igts applied for each tree.
            impurities : float[n_trees][N]

        """
        if len(igts)!= 0:
            self.igts_applied[0, self.kc] = np.quantile(igts, 0)    # Q0 <=> min
            self.igts_applied[1, self.kc] = np.quantile(igts, 0.25) # Q1
            self.igts_applied[2, self.kc] = np.quantile(igts, 0.5)  # Q2 <=> median
            self.igts_applied[3, self.kc] = np.quantile(igts, 0.75) # Q3
            self.igts_applied[4, self.kc] = np.quantile(igts, 1)    # Q4 <=> max

        impurities = np.concatenate(impurities)
        if len(impurities)!= 0:
            self.impurities[0, self.kc] = np.quantile(impurities, 0)    # Q0 <=> min
            self.impurities[1, self.kc] = np.quantile(impurities, 0.25) # Q1
            self.impurities[2, self.kc] = np.quantile(impurities, 0.5)  # Q2 <=> median
            self.impurities[3, self.kc] = np.quantile(impurities, 0.75) # Q3
            self.impurities[4, self.kc] = np.quantile(impurities, 1)    # Q4 <=> max

    def addImprovementCycleResult(self, forest):
        """Add max_depth and nb_leaves result of current improvement.

        Args
        ----
            forest: CustomRandomForestClassifier.

        """
        max_depth = np.fromiter(( t.tree.max_depth for t in forest.estimators_), np.uint8)

        self.depths = np.append(self.depths, (np.quantile(max_depth,0),
                            np.quantile(max_depth,0.25),
                            np.quantile(max_depth,0.5),
                            np.quantile(max_depth,0.75),
                            np.quantile(max_depth,1))
                            )


        leaves = np.fromiter(( np.sum(np.array(t.tree.feature == -2)) for t in forest.estimators_), np.uint64)

        self.leaves = np.append(self.leaves, (np.quantile(leaves,0),
                                    np.quantile(leaves,0.25),
                                    np.quantile(leaves,0.5),
                                    np.quantile(leaves,0.75),
                                    np.quantile(leaves,1)))

        self.kc += 1



    def addImprovementCycleResultConfusionMatrix(self, mc_classes):
        """Add confusion matrix of misclassified result of current improvement.

        Args
        ----
            mc_classes: confusion matrix of misclassified samples shape = (n_classes, n_classes).

        """
        self.confusion_matrix = np.append(self.confusion_matrix, mc_classes)

    def addImprovementCycleResultMisclassifiedManu(self, mc_indices):
        """Add misclassified samples indices of current improvement.

        Args
        ----
            mc_indices: array of misclassified samples indices.

        """
        self.mc_manu_indices = np.append(self.mc_manu_indices, mc_indices)


    def addConsolidationCycleResult(self, forest_sbs, acc_typo, acc_manu):
        """Add LTM accuracy score on Test dataset for each class of current consolidation and add current model weight.

        Args
        ----
            forest: CustomRandomForestClassifier type with all trees.
            forest_sbs: CustomRandomForestClassifier type with portion of trees selected by SBS method.
            predicted_typo: array of length n_classes of printed numbers performances
            predicted_manu: array of length n_classes of handwritten numbers performances

        """
        if acc_typo is None:

            self.consolidations_typo[self.c] = self.consolidations_typo[self.c - 1]
            self.consolidations_manu[self.c] = self.consolidations_manu[self.c - 1]

        else:
            self.consolidations_typo[self.c] = acc_typo

            self.consolidations_manu[self.c] = acc_manu

        self.size_of_forest_sbs = np.append(self.size_of_forest_sbs, get_size(forest_sbs))

        self.c += 1


def fromReportToExcel(report, out_file):
    """Create an Excel file of the results of a Report object.

    Args
    ----
        report: Report type object.
        out_file: Filename of returned xlsx file.

    """
    leaves = report.leaves.reshape(report.kc, 5)
    depths = report.depths.reshape(report.kc, 5)
    # calculate number of misclassified at each improvement with confusion matrix
    mc = np.array([np.sum(conf_m - np.diag(np.diag(conf_m))) for conf_m in report.confusion_matrix.reshape(report.kc, n_classes, n_classes)]).reshape(-1)
    width = len(leaves)
    # Building first sheet of excel file
    df1 = DataFrame({'leaves_min': leaves[:width,0],
                       'leaves_q1':  leaves[:width,1],
                        'leaves_median': leaves[:width,2],
                        'leaves_q3': leaves[:width,3],
                        'leaves_max': leaves[:width,4],
                        'depths_min':  depths[:width,0],
                        'depths_q1': depths[:width,1],
                        'depths_median': depths[:width,2],
                        'depths_q3': depths[:width,3],
                        'depths_max': depths[:width,4],
                        'igt_min': report.igts_applied[0,:width],
                        'igt_q1': report.igts_applied[1,:width],
                        'igt_median': report.igts_applied[2,:width],
                        'igt_q3': report.igts_applied[3,:width],
                        'igt_max': report.igts_applied[4,:width],
                        'impurity_min': report.impurities[0,:width],
                        'impurity_q1': report.impurities[1,:width],
                        'impurity_median': report.impurities[2,:width],
                        'impurity_q3': report.impurities[3,:width],
                        'impurity_max': report.impurities[4,:width],
                        'mc': mc
                        })

    # calculate global accuracy-score with accuracy-score per class
    perfs_manu = np.average(report.consolidations_manu, axis = 1, weights = report.classes_weights_typo )
    perfs_typo = np.average(report.consolidations_typo, axis = 1, weights = report.classes_weights_manu )

    # Building second sheet of excel file
    df2 = DataFrame({'global_accuracy_typo':perfs_typo,
                              'global_accuracy_manu':perfs_manu,
                              'accuracy_typo_0':report.consolidations_typo[:,0],
                              'accuracy_typo_1':report.consolidations_typo[:,1],
                              'accuracy_typo_2':report.consolidations_typo[:,2],
                              'accuracy_typo_3':report.consolidations_typo[:,3],
                              'accuracy_typo_4':report.consolidations_typo[:,4],
                              'accuracy_typo_5':report.consolidations_typo[:,5],
                              'accuracy_typo_6':report.consolidations_typo[:,6],
                              'accuracy_typo_7':report.consolidations_typo[:,7],
                              'accuracy_typo_8':report.consolidations_typo[:,8],
                              'accuracy_typo_9':report.consolidations_typo[:,9],
                              'accuracy_manu_0':report.consolidations_manu[:,0],
                              'accuracy_manu_1':report.consolidations_manu[:,1],
                              'accuracy_manu_2':report.consolidations_manu[:,2],
                              'accuracy_manu_3':report.consolidations_manu[:,3],
                              'accuracy_manu_4':report.consolidations_manu[:,4],
                              'accuracy_manu_5':report.consolidations_manu[:,5],
                              'accuracy_manu_6':report.consolidations_manu[:,6],
                              'accuracy_manu_7':report.consolidations_manu[:,7],
                              'accuracy_manu_8':report.consolidations_manu[:,8],
                              'accuracy_manu_9':report.consolidations_manu[:,9],
                              'incremental_data_passed': np.arange(len(perfs_typo)) * report.K * report.N,
                              'size of forest with sbs' : report.size_of_forest_sbs
                              })

    writer = ExcelWriter(out_file)
    for i, df in enumerate([df1,df2]):
        df.to_excel(writer,'sheet{}'.format(i))
    writer.save()


class ReportsUtil :
    """Enables to add results from all other singletons in the program. This is particularly usefull for GridSearch.

    Attributes
    ----------
    reports: array
        All reports of a sequence of learning experiments
    current_report: Report
        This is the current Report wich receives all results

    """

    class __ReportsUtil:

        def __init__(self):

            self.reports = []
            self.current_report = None

        def setCurrentReport(self, report):
            """Change current report for a new learning experiment.

            Args
            ----
                report: Report object type to add in reports list.

            """
            if self.current_report is not None:
                self.reports.append(copy.deepcopy(self.current_report))
            self.current_report = report

    instance = None

    def __new__(self):
        """Singleton init."""
        if not ReportsUtil.instance:
            ReportsUtil.instance = ReportsUtil.__ReportsUtil()
        return ReportsUtil.instance




