# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 15:45:41 2020

@author: Labo

CALCULER EQM
"""

from Configuration import tTrain_path, ltTrain_path, colors
import pickle
import seaborn as sns
import numpy as np
import math
from scipy.stats import norm
import matplotlib.pyplot as plt

plt.style.use('seaborn-white')
kwargs = dict(histtype='stepfilled', alpha=0.3, edgecolor='blue')


def gauss(data, sigma, mu, step):
    gauss_res = norm(mu, sigma).pdf(data)
    return gauss_res/sum(gauss_res*step)

def get_distribs(dist, X, Y, c, f, step, plot = False):
    data = X[Y == c][:,f]
    bins = int((max(data) - min(data))/step)

    mu = dist[c][f][0]
    sigma = math.sqrt(dist[c][f][1])
    
    hist = np.histogram(data,bins)
    x = [(hist[1][i+1] + hist[1][i])/2 for i in range(len(hist[1]) -1)]
    y = hist[0]/len(data)
    y = y / sum(y*step)
    

    if plot:
        plt.figure(figsize=(15,8))
        plt.rcParams.update({'font.size': 18})

        plt.hist(x, bins = bins, weights = y, **kwargs)
        plt.plot(x,
                gauss(x, sigma, mu, step),
                color='red',
                label = "Loi normale (mu=" + str(round(mu,2)) + ", sigma=" + str(round(sigma,2)) + ")")
        plt.xlabel("Classe " + str(c) + "  |  Feature " + str(f))
        plt.ylabel("Probabilité")
        plt.legend()
    
    return x, y, mu, sigma

with open(tTrain_path, "rb") as bin_f_data:
   with open(ltTrain_path, "rb") as bin_f_label:
       TrainX = pickle.load(bin_f_data)
       TrainY = pickle.load(bin_f_label)

with open("global_distrib_mean_var.sav", "rb") as f_dist:
    dist = pickle.load(f_dist)


step = 0.05
EQM = np.zeros((256,10))
boxplots = []
for c in range(10):
    for f in range(256):
        x, y, mu, sigma = get_distribs(dist, TrainX, TrainY, c, f, step)
        gauss_res = gauss(x, sigma, mu, step)
        epsilons = (gauss_res - y)**2
        EQM[f,c] = np.mean(epsilons)

    boxplots.append(EQM[:,c])


fig = plt.figure(1, figsize=(16,8))
plt.rcParams.update({'font.size': 14})
ax = fig.add_subplot(111)
ax.set_xticklabels(range(10))
ax.set_xlabel("Classe")
ax.set_ylabel("EQM")
bp = ax.boxplot(boxplots)





