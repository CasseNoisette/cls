from distutils.core import setup
from Cython.Build import cythonize
import numpy, os

path = "mytree.pyx"
setup(ext_modules=cythonize(path), include_dirs=[numpy.get_include()])