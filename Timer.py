# -*- coding: utf-8 -*-
"""
Created on Tue Jan 21 13:53:10 2020

@author: bourdoulous berangere


Classe qui permet entre autre de récupérer le temps écoulé depuis le début d'un
apprentissage incrémental
"""

import time

class Timer:
    
    class __Timer:
        
        def __init__(self):
            
            self.start = time.time()
        
        def getTime(self):
            return time.time() - self.start
            
    
    instance = None

    def __new__(self):
        if not Timer.instance:
            Timer.instance = Timer.__Timer()
        return Timer.instance