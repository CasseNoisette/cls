# -*- coding: utf-8 -*-
"""
Created on Wed Oct 30 14:45:22 2019

@author: Z240
"""
from keras.models import load_model, Model
from keras.datasets import mnist
from keras import backend as K
import numpy as np
import seaborn as sns
import math
import matplotlib.pyplot as plt
import pickle
from matplotlib.ticker import FixedFormatter
from Custom_RF import CustomRandomForestClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from Configuration import tTrain_path,  ltTrain_path, \
             mTrain_path,  lmTrain_path
from sklearn.mixture import GaussianMixture

class CNN:

    def __init__(self, X):
        self.features_extractor_path = './CNN.h5'
        self.CNN = load_model(self.features_extractor_path)
        self.intermediate_layer_model = None


        for layer in self.CNN.get_config().get("layers") :
            if layer.get("class_name")=='Flatten' :
                CNN_features_layer = layer.get('config').get('name')
                self.intermediate_layer_model = Model(inputs=self.CNN.input,
                                         outputs=self.CNN.get_layer(CNN_features_layer).output)
                f = self.intermediate_layer_model.predict(X)
                self.__pca = PCAresult(f)



    def getFeatures(self, data):
        if self.intermediate_layer_model != None :
            features=self.intermediate_layer_model.predict(data)
            features = self.__pca.base_A_to_B(features)
            return features
        else :
            print("Error : no Flatten layer")
            exit()

class PCAresult:
    def __init__(self, X):
        pca = PCA(n_components = X.shape[1])
        pca.fit(X)
        self.P = pca.components_
        self.P_inv = np.linalg.inv(self.P)

    def base_A_to_B(self, X_A):
        return np.array([ np.dot(self.P_inv, np.dot(x, self.P)) for x in X_A])

    def base_B_to_A(self, X_B):
        return np.array([ np.dot(self.P, np.dot(x, self.P_inv)) for x in X_B])


class Socle :
    def __init__(self) :
        self.RF = None  #pickle.load(open(self.RF_path, 'rb'))
        self.CNN = None



    def loadDataset(self, dataset, categorial=True):
        if dataset == "TYPO" :
            DATA_TRAIN = '../../cls_datasets/typo_dataset_one_hot_encoding/data_train.bin'
            DATA_TEST = '../../cls_datasets/typo_dataset_one_hot_encoding/data_test.bin'
            LABEL_TRAIN = '../../cls_datasets/typo_dataset_one_hot_encoding/label_train.bin'
            LABEL_TEST = '../../cls_datasets/typo_dataset_one_hot_encoding/label_test.bin'

            # Chargement des donnees typo
            with open(DATA_TRAIN, "r") as bin_f_data:
                with open(LABEL_TRAIN, "r") as bin_f_label:
                    TrainX = np.fromfile(bin_f_data, dtype='float64')
                    size = len(TrainX)//(28*28)
                    TrainX = TrainX.reshape(size,28,28,1)
                    TrainY = np.fromfile(bin_f_label, dtype='uint8').reshape(size, 10)
                    if categorial :
                        # Transformation one hot vers categorical
                        TrainY_lab=[]
                        for t in TrainY :
                            TrainY_lab.append(np.argmax(t))
                        TrainY=np.array(TrainY_lab)

#            with open(DATA_TEST, "r") as bin_f_data:
#                with open(LABEL_TEST, "r") as bin_f_label:
#                    TestX = np.fromfile(bin_f_data, dtype='float64')
#                    size = len(TestX)//(28*28)
#                    TestX = TestX.reshape(size,28,28,1)
#                    TestY = np.fromfile(bin_f_label, dtype='uint8').reshape(size, 10)
#                    if categorial :
#                        # Transformation one hot vers categorical
#                        TestY_lab=[]
#                        for t in TestY :
#                            TestY_lab.append(np.argmax(t))
#                        TestY=np.array(TestY_lab)

            self.CNN = CNN(TrainX)
            return self.CNN.getFeatures(TrainX), TrainY #, self.CNN.getFeatures(TestX), TestY
        else :
            # input image dimensions
            img_rows, img_cols = 28, 28

            # the data, split between train and test sets
            (x_train, y_train), (x_test, y_test) = mnist.load_data()

            if K.image_data_format() == 'channels_first':
                x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
                x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
            else:
                x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
                x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)

            x_train =1-x_train.astype('float32')
            x_test = 1-x_test.astype('float32')
            x_train /= 255
            x_test /= 255

            return self.CNN.getFeatures(x_train), y_train, self.CNN.getFeatures(x_test), y_test




    def computeDistribution(self, X, Y, state) : #else 'update'
        if state == 'init':
            Y_ = Y.astype(dtype='str').reshape(Y.shape[0], 1)
            dataset = np.concatenate((X, Y_), axis=1)
            self.data_distrib = {}
            # format : {classe : {feature : (barycentre, variance)}}
            for c in np.unique(Y):
                distrib_feat = {}
                data_c = X[Y == c]
                for f in range(0, X.shape[1]):
                    sub_dataset = data_c[:,f]

                    gm = GaussianMixture()
                    gm.fit(sub_dataset.reshape(-1,1))

                    distrib_feat[f] = (gm.means_[0][0], gm.covariances_[0][0][0])
                self.data_distrib[c]=distrib_feat
            new = dict()
            for c in np.unique(Y):
                new[c] = np.array([ [ self.data_distrib[c][f][0] for f in range(X.shape[1])],
            [ self.data_distrib[c][f][1] for f in range(X.shape[1])]])
            self.data_distrib = new


    def printDistrib(self, X, Y) :
        Y_ = Y.astype(dtype='str').reshape(Y.shape[0], 1)
        dataset = np.concatenate((X, Y_), axis=1)

        fig, axes = plt.subplots(16, 16, figsize=[29.7,42])
        sns.set_palette("Paired")
        i=0
        j=0

        for f, cpt in enumerate(range(0, X.shape[1]), 0):
            feat_sub = dataset[:, f]
            if cpt/16 in range(1,18) :
                i+=1
                j=0
            for c in np.unique(Y) :
                sub = np.where(dataset[:,-1] == c)
                sub_dataset=np.array(feat_sub[list(sub[0])]).astype(np.float)
                if i == 0 and j ==7 :
                    plot_lab = sns.kdeplot(sub_dataset, label=str(c), ax=axes[i, j], bw=0.4)
                    plot=plot_lab
                else :
                    plot = sns.kdeplot(sub_dataset, ax=axes[i, j], bw=0.4)

            plot.text(2,0.9, ("F" + str(f)))
            plot.set_ylim(0,1)
            plot.set_frame_on(True)
            y_formatter = FixedFormatter(["0", ".2", ".4", ".6", ".8", "1"])
            plot.yaxis.set_major_formatter(y_formatter)
            plt.xticks(fontsize=8)
            plt.yticks(fontsize=8)
            j+=1

        plot_lab.legend(loc='upper center', bbox_to_anchor=(0.5, 1.5), shadow=True, ncol=10)
        fig.suptitle("Distribution des 10 classes (typo+manu) selon les 256 features en sortie de CNN",
                     y= 0.91, verticalalignment = 'baseline', fontsize='xx-large')

        plt.show()



    def score(self, X, Y):
        X_features = self.__getFeatures(X)
        score = self.RF.score(X_features, Y)
        return score



    def predict(self, X):
        X_features = self.__getFeatures(X)
        pred = self.RF.predict(X_features)
        return pred


def multi_training(self, X, Y, Xtest, Ytest, nb_estimators = 100, max_depth = 22):
    print("Entrainement d'une foret aleatoire de %d arbres et %d de profondeur..."%(nb_estimators, max_depth))

    #X=np.array(self.__getFeatures(X))
    bscore = 0
    max_features = math.floor(math.sqrt(X.shape[1]))

    for i in range(0,10):
        print("It : ", i)
        forest = CustomRandomForestClassifier(n_estimators = nb_estimators,
                                              max_features = max_features,
                                              max_depth = max_depth,
                                              verbose = 2,
                                              max_samples = 0.9,
                                              n_jobs = 4)
        forest.fit(X, Y)

        score = accuracy_score(forest.predict(Xtest), Ytest)
        print("Forest test score %f"%(score))

        if(score > bscore):
            best_RF = forest
            bscore = score

    return best_RF


def saveRF(forest):
    with open('./RandomForest.sav', 'wb') as f:
        pickle.dump(forest, f)

    print("Sauvegarde parametres : ok")

def saveDistrib(data_distrib):
    with open('./global_distrib_mean_var.sav', 'wb') as f:
        pickle.dump(data_distrib, f)



def loadRF(filename):
    with open(filename, 'rb') as f:
        print("Loading forest...")
        return pickle.load(f)


def saveConvertedData(filename, data):
    with open(filename,'wb') as f:
        pickle.dump(data, f)



    

if __name__ == "__main__":
    model = Socle()
    
    trainX, trainY= model.loadDataset('TYPO')
    # trainX is already converted into 256 independent features
    model.CNN.getFeatures(images)
    
    
#    with open(cnn_acp_file, "wb") as file_:
#        pickle.dump(model.CNN, file_)
    #MX, MY, TX, TY = model.loadDataset('MANU')
#
#    f_ = RandomForestClassifier(100, max_depth = 27, max_samples=0.9)
#    f_.fit(np.vstack((trainX, MX)), np.concatenate((trainY,MY)))
#
#    accuracy_score(f_.predict(testX), testY)
#    accuracy_score(f_.predict(TX), TY)


#    model.computeDistribution(trainX,trainY, 'init')
#    saveDistrib(model.data_distrib)


    #saveConvertedData(tTrain_path, trainX)
    #saveConvertedData(mTrain_path, MX)
    #saveConvertedData(tTest_path, testX)
    #saveConvertedData(mTest_path, TX)
    #
    #saveConvertedData(ltTrain_path, trainY)
    #saveConvertedData(lmTrain_path, MY)
    #saveConvertedData(ltTest_path, testY)
    #saveConvertedData(lmTest_path, TY)


    #f = loadRF('./RandomForestMon_Dec_16_02_09_35_2019.sav')
    #best_RF = model.multi_training(trainX, trainY, testX, testY)
    #5saveRF(best_RF)

#from Configuration import tTrain_path, tTest_path, ltTrain_path, \
#                          ltTest_path, mTrain_path, mTest_path, lmTrain_path, lmTest_path
#print('\tLoading typo dataset...')
#with open(tTrain_path, "rb") as bin_f_data:
#    with open(ltTrain_path, "rb") as bin_f_label:
#        TrainX = pickle.load(bin_f_data)
#        TrainY = pickle.load(bin_f_label)
#
#with open(tTest_path, "rb") as bin_f_data:
#    with open(ltTest_path, "rb") as bin_f_label:
#        TestX = pickle.load(bin_f_data)
#        TestY = pickle.load(bin_f_label)
#
#tTrain = (TrainX, TrainY)
#tTest = (TestX, TestY)
#
#print('\tLoading Manu dataset...')
#with open(mTrain_path, "rb") as bin_f_data:
#    with open(lmTrain_path, "rb") as bin_f_label:
#        TrainX = pickle.load(bin_f_data)
#        TrainY = pickle.load(bin_f_label)
#
#with open(mTest_path, "rb") as bin_f_data:
#    with open(lmTest_path, "rb") as bin_f_label:
#        TestX = pickle.load(bin_f_data)
#        TestY = pickle.load(bin_f_label)
#
#mTrain = (TrainX, TrainY)
#mTest = (TestX, TestY)
#
#forest_ = RandomForestClassifier(n_estimators = 100, max_depth = 22)
#forest_.fit(mTrain[0], mTrain[1])
#print("perf manu", accuracy_score(forest_.predict(mTest[0]), mTest[1]))
#print("perf typo", accuracy_score(forest_.predict(tTest[0]), tTest[1]))
#
#forest_ = RandomForestClassifier(n_estimators = 100, max_depth = 22)
#forest_.fit(np.vstack((mTrain[0], tTrain[0])),np.vstack(( mTrain[1], tTrain[1])))
#print("perf manu", accuracy_score(forest_.predict(mTest[0]), mTest[1]))
#print("perf typo", accuracy_score(forest_.predict(tTest[0]), tTest[1]))
