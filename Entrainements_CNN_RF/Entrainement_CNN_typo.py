# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 15:41:14 2019

@author: Z240
"""

from sklearn.model_selection import train_test_split as split
from sklearn.metrics import accuracy_score, confusion_matrix
from keras import callbacks
import numpy as np
import Archi
from matplotlib import pyplot
from keras.utils import plot_model

def load_data():
    DATA_TRAIN = './cls_datasets/typo_dataset_one_hot_encoding/data_train.bin'
    DATA_TEST = './cls_datasets/typo_dataset_one_hot_encoding/data_test.bin'
    LABEL_TRAIN = './cls_datasets/typo_dataset_one_hot_encoding/label_train.bin'
    LABEL_TEST = './cls_datasets/typo_dataset_one_hot_encoding/label_test.bin'
    
    # Chargement des donnees typo
    with open(DATA_TRAIN, "r") as bin_f_data:
        with open(LABEL_TRAIN, "r") as bin_f_label:
            TrainX = np.fromfile(bin_f_data, dtype='float64')
            size = len(TrainX)//(28*28)
            TrainX = TrainX.reshape(size,28,28,1)
            TrainY = np.fromfile(bin_f_label, dtype='uint8').reshape(size, 10)
            
    with open(DATA_TEST, "r") as bin_f_data:
        with open(LABEL_TEST, "r") as bin_f_label:
            TestX = np.fromfile(bin_f_data, dtype='float64')
            size = len(TestX)//(28*28)
            TestX = TestX.reshape(size,28,28,1)
            TestY = np.fromfile(bin_f_label, dtype='uint8').reshape(size, 10)
            
    return TrainX, TrainY, TestX, TestY

model = Archi.CNN0()
trainX, trainY, testX, testY = load_data()

ES=callbacks.EarlyStopping(monitor='val_accuracy', 
                           mode='max')
MC=callbacks.ModelCheckpoint('best_bdd_OK.h5',
                             monitor='val_accuracy', 
                             mode='max',
                             save_best_only=True)

history = model.fit(trainX, trainY, 
          epochs=100, 
          batch_size=128, 
          verbose=1, 
          validation_split=0.1,
          callbacks=[ES, MC])

model.evaluate(trainX, trainY, batch_size=128, verbose=1)

predictions = np.around(model.predict(testX, batch_size=128)).astype(int)
print("Prediction sur base de test : ", accuracy_score(testY, predictions)*100, ' %')

testY_lab=[]
predictions_lab=[]
for target, pred in zip(testY, predictions):
    testY_lab.append(np.argmax(target))
    predictions_lab.append(np.argmax(pred))
np.array(testY_lab)
np.array(predictions_lab)

print(confusion_matrix(testY_lab, predictions_lab))

pyplot.plot(history.history['loss'], label='train')
pyplot.plot(history.history['val_loss'], label='test')
pyplot.title('Model loss')
pyplot.ylabel('Loss')
pyplot.xlabel('Epoch')
pyplot.legend()
pyplot.show()

pyplot.plot(history.history['accuracy'], label='train')
pyplot.plot(history.history['val_accuracy'], label='test')
pyplot.title('Model accuracy')
pyplot.ylabel('Accuracy')
pyplot.xlabel('Epoch')
pyplot.legend()
pyplot.show()

plot_model(model, to_file='model_CNN0.png')

