# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 14:38:34 2019

@author: Z240
"""

from keras.models import load_model, Model
import numpy as np
import math
from sklearn.model_selection import train_test_split as split
from sklearn.ensemble import RandomForestClassifier as RFC
import matplotlib.pyplot as plt
import pickle
import pandas as pd

def load_data():
    DATA_TRAIN = '.././cls_datasets/typo_dataset_one_hot_encoding/data_train.bin'
    DATA_TEST = '.././cls_datasets/typo_dataset_one_hot_encoding/data_test.bin'
    LABEL_TRAIN = '.././cls_datasets/typo_dataset_one_hot_encoding/label_train.bin'
    LABEL_TEST = '.././cls_datasets/typo_dataset_one_hot_encoding/label_test.bin'

    # Chargement des donnees typo
    with open(DATA_TRAIN, "r") as bin_f_data:
        with open(LABEL_TRAIN, "r") as bin_f_label:
            TrainX = np.fromfile(bin_f_data, dtype='float64')
            size = len(TrainX)//(28*28)
            TrainX = TrainX.reshape(size,28,28,1)
            TrainY = np.fromfile(bin_f_label, dtype='uint8').reshape(size, 10)
            # Transformation one hot vers categorical
            TrainY_lab=[]
            for t in TrainY :
                TrainY_lab.append(np.argmax(t))
            TrainY=np.array(TrainY_lab)

    with open(DATA_TEST, "r") as bin_f_data:
        with open(LABEL_TEST, "r") as bin_f_label:
            TestX = np.fromfile(bin_f_data, dtype='float64')
            size = len(TestX)//(28*28)
            TestX = TestX.reshape(size,28,28,1)
            TestY = np.fromfile(bin_f_label, dtype='uint8').reshape(size, 10)
            # Transformation one hot vers categorical
            TestY_lab=[]
            for t in TestY :
                TestY_lab.append(np.argmax(t))
            TestY=np.array(TestY_lab)

    return TrainX, TrainY, TestX, TestY

def getFeaturesLayerName(model):
    layer_name=None
    conf = model.get_config().get("layers")
    for layer in conf :
        if layer.get("class_name")=='Flatten' :
            layer_name = layer.get('config').get('name')

    print("Features layer name : ", layer_name)

    return layer_name

def getFeatures(data, layer_name):

    if layer_name != None :
        intermediate_layer_model = Model(inputs=model.input,
                                     outputs=model.get_layer(layer_name).output)
        features=intermediate_layer_model.predict(data)
        return features
    else :
        print("Error : no Flatten layer")
        exit()

def GridSearch(data, labels, nb_tree=range(60,140,10), max_depth=range(14,32,2)):
    train, val, train_labels, val_labels = split(data, labels, train_size=0.9)
    best_score = 0
    for tree in nb_tree :
        recap[tree]=[]
        for depth in max_depth :
            print("NB_ARBRES : ", tree)
            print("DEPTH : ", depth)
            clf = RFC(n_estimators=tree, max_depth=depth,
                     max_features=int(math.sqrt(data.shape[1])),
                     oob_score=True)
            clf.fit(train, train_labels)
            score = clf.score(val, val_labels)
            recap[tree].append(score)
            print("score=",score)
            if score > best_score :
                best_tree = tree
                best_depth=depth
                best_score=score
                best_clf = clf

    print("Parametres optimaux : ")
    print("NB_TREE == ", best_tree)
    print("MAX_DEPTH == ", best_depth)
    print("Pour un score validation de : ", best_score)

    for k, v in recap.items():
        plt.plot(list(max_depth), v, linestyle='--', label=(str(k) +" arbres"))
    plt.legend()
    plt.xlabel("Max depth")
    plt.ylabel("Accuracy sur validation")
    plt.title("Resultat grid search")
    plt.grid()
    plt.show()

    return best_clf

def entrainement(train, train_labels, nb_tree=90, max_depth=20):
    print("Entrainement d'une foret aleatoire de %d arbres et %d de profondeur..."%(nb_tree, max_depth))
    train, test, train_labels, tst_labels = split(train, train_labels, train_size=0.9)
    bscore = 0
    for i in range(0, 10):
        print("It : ", i)
        clf = RFC(n_estimators=nb_tree, max_depth=max_depth,
                     max_features=int(math.sqrt(train.shape[1])),
                     oob_score=True)
        clf.fit(train, train_labels)
        score = clf.score(test, tst_labels)

        if score > bscore :
            bscore=score
            best_clf = clf

    print("Score train : ", best_clf.score(train, train_labels))
    print("Score validation : ", bscore)

    return best_clf

def trainTransfere(data, labels, test, tst_labels, param = False):
    layer_name=getFeaturesLayerName(model)
    # Extraction des features
    data_features = getFeatures(data, layer_name)
    test_features = getFeatures(test, layer_name)

    if param == True :
        # Choix des parametres de la foret
        foret = GridSearch(data_features, labels)
    else :
        # Construction de la foret
        foret = entrainement(data_features, labels)

    score = foret.score(test_features, tst_labels)

    print("Score test : ", score)

    return foret



def saveParams(forest):
    with open('./RandomForest(2).sav', 'wb') as f:
        pickle.dump(forest, f)

    print("Sauvegarde parametres : ok")


recap={}
if __name__ == "__main__":
    # Chargement CNN
    model = load_model('./CNN.h5')

    # Chargement données typo
    trainX, trainY, testX, testY = load_data()



    # Creation du modele
    RF=trainTransfere(trainX, trainY, testX, testY, True)
    saveParams(RF)


