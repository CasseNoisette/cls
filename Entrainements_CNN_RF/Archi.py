# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 16:55:55 2019

@author: Z240
"""

from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D

def CNN0() :
    model=Sequential()
    
    # format input data : (taille_app, 28, 28, 1)
    
    model.add(Conv2D(6, kernel_size=(5,5),
                     strides=(1,1), 
                     padding='valid',  #no padding
                     data_format='channels_last',
                     activation='relu', input_shape=(28,28,1))) 
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(16, kernel_size=(5,5),
                     strides=(1,1),
                     activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    #model.add(Dense(84, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    return model


def ConvNet():
    model=Sequential()
    
    # format input data : (taille_app, 28, 28, 1)
    
    model.add(Conv2D(16, kernel_size=(7,7),
                     strides=(1,1), 
                     padding='valid',  #no padding
                     data_format='channels_last',
                     activation='relu', input_shape=(28,28,1))) 
    model.add(Conv2D(16, kernel_size=(7,7),
                     strides=(1,1), 
                     padding='valid',  #no padding
                     activation='relu')) 
    model.add(MaxPooling2D(pool_size=(6,6), padding='same'))
    
    model.add(Conv2D(16, kernel_size=(7,7),
                     strides=(1,1),
                     padding='same',
                     activation='relu'))
    model.add(Conv2D(16, kernel_size=(7,7),
                     strides=(1,1), 
                     padding='same',  # padding
                     activation='relu')) 
    model.add(MaxPooling2D(pool_size=(6,6), padding='same'))
    
    model.add(Conv2D(16, kernel_size=(7,7),
                     strides=(1,1), 
                     padding='same',  # padding
                     activation='relu')) 
    model.add(Conv2D(16, kernel_size=(7,7),
                     strides=(1,1), 
                     padding='same',  # padding
                     activation='relu')) 
    model.add(MaxPooling2D(pool_size=(6,6), padding='same'))
    
    model.add(Flatten())
    model.add(Dropout(0.5))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    #model.add(Dense(84, activation='relu'))
    model.add(Dense(10, activation='softmax'))
    
    model.compile(loss='crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    return model
