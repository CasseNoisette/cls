# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 12:40:40 2019

@author: Z240
"""

from keras.models import load_model
from keras.datasets import mnist
from keras import backend as K
import keras.utils
import numpy as np
from sklearn.metrics import accuracy_score, confusion_matrix

model = load_model('./CNN.h5')

def load_MNIST():
    # input image dimensions
    img_rows, img_cols = 28, 28
    
    # the data, split between train and test sets
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    
    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)
    
    x_train = 1-x_train.astype('float32')
    x_test = 1-x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    
    # convert class vectors to binary class matrices
    y_train = keras.utils.to_categorical(y_train, 10)
    y_test = keras.utils.to_categorical(y_test, 10)
    
    
    X = np.concatenate((x_train,x_test))
    Y = np.concatenate((y_train,y_test))
    
    return X, Y

X, Y = load_MNIST()

predictions = np.around(model.predict(X, batch_size=128)).astype(int)
print("Prediction MNIST : ", accuracy_score(Y, predictions)*100, ' %')

testY_lab=[]
predictions_lab=[]
for target, pred in zip(Y, predictions):
    testY_lab.append(np.argmax(target))
    predictions_lab.append(np.argmax(pred))
np.array(testY_lab)
np.array(predictions_lab)