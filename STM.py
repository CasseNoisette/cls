# -*- coding: utf-8 -*-
"""
Created on Sun Dec  8 01:39:34 2019.

@author: bourd
"""

from Datas import DaS
from LTM import LTM
import numpy as np
import copy
from sklearn.metrics import confusion_matrix
from Configuration import Config, n_classes
from CustomDecisionTreeClassifier import TreeUtils
from GraphicReport import ReportsUtil

class STM :
    '''
    Attributes:
        empanMnesique: retrieves data from the flow (Empan_Mnesique)
        mdt: updates stm forest (MDT)
        __RFs: contains KxN stm forests at the end of "improvement cycle" array(CustomRandomForestClassifier)
    '''

    def __init__(self, forest):
        print("STM __init__: ...")
        self.empanMnesique = Empan_Mnesique()
        self.mdt = MDT(forest)

    def launchOneIncrementalLearningCycle(self):
        best_stm_index = 0
        self.__RFs = dict()
        self.__RFs[0] = copy.deepcopy(self.mdt.RF)

        for k in range(1, Config().K + 1):
            print("\tSTM __learning__: k =", k)
            ech, weights = self.empanMnesique.getEchantillon()

            # get misclassified hanwritten numbers
            mci = self.getMC(ech)
            print("\tSTM __learning__: total wrong predictions " + str(len(mci)) + "/" + str(len(ech[0])))

            if mci.size > 0:
                # update trees with misclassified
                self.mdt.updateRF(ech[0][mci], ech[1][mci], weights)

                self.__RFs[k] = copy.deepcopy(self.mdt.RF)

                # compare perfs
                mVal = DaS().getMVal()

                best_stm_index = self.perfectionnement(mVal)

                self.mdt.setRF(self.__RFs[best_stm_index])

        LTM().consolidate(self.__RFs[best_stm_index])

    def getMC(self, ech):
        predictions = self.mdt.RF.predict(ech[0])

        mc_index = np.argwhere(predictions != ech[1].reshape(-1,)).reshape(-1)

        MC_indices = mc_index + DaS().ech_it * Config().N

        conf = confusion_matrix(ech[1], predictions, labels = range(10))

        ReportsUtil().current_report.addImprovementCycleResultConfusionMatrix(conf)
        ReportsUtil().current_report.addImprovementCycleResultMisclassifiedManu(MC_indices)
        return mc_index


    def getGlobalDistrib(self):
        return LTM().getDistribution()

    def getRFs(self):
        return self.__RFs

    def perfectionnement(self, mVal):

        mValX = mVal[0]
        mValY = mVal[1]
        best_perf = 0
        best_index = 0
        print("\tImprovement on mVal (size=" + str(len(mValX)) + ") for " + str(len(self.__RFs)) + " STMs")
        perfs = []
        for rf_k, rf in self.__RFs.items():
            sc = rf.score(mValX, mValY)
            perfs.append((rf_k, sc))

        perfs=np.array(perfs)
        idx_max = np.argmax(perfs[:,1])

        #idx_max
        if perfs[idx_max][1] > best_perf :
            best_perf = perfs[idx_max][1]
            best_index = perfs[idx_max][0]
        print("\t\t Best STM index is " + str(int(best_index)) + " with score " + str(best_perf) )

        ReportsUtil().current_report.addImprovementCycleResult(self.__RFs[best_index])
        return best_index



class MDT:
    def __init__(self, forest):
        if forest == None:
            raise Exception("Forest has NoneType")
        self.RF = forest
        print("\tMDT __init__: OK")


    def setRF(self, new_RF):
        self.RF =  copy.deepcopy(new_RF)

    def updateRF(self, mc, mc_lab, weights):
        self.chooseStrategy(mc, mc_lab, weights)

    def chooseStrategy(self, mc, mc_lab, weights):
        ## ULS IGT
        dist = LTM().getTypoDistribution()
        trees = self.RF.estimators_

        # igts = Parallel(n_jobs=self.RF.n_jobs, verbose=self.RF.verbose,backend="threading")(
        #     delayed( TreeUtils.update_leaves)(it, tree, mc, mc_lab, weights, dist)
        #     for it, tree in enumerate(trees))
        res = np.array([TreeUtils.update_leaves(it,tree, mc, mc_lab, weights, dist) for it,tree in enumerate(trees)])
        ReportsUtil().current_report.update_igts_and_impurities(res[:,0], res[:,1])


class Empan_Mnesique:

    def __init__(self):
        self.classes_weights = np.array([ Config().W_init ] * n_classes)
        self.classes_cpt = np.zeros((n_classes,), dtype=np.uint64)
        print("\tEmpanMnesique __init__: OK")

    def getEchantillon(self):
        """
        Return incremental dataset from flow.

        Returns
        -------
        ech : (X, Y) with X : float[N][n_features] & Y : float[N]
            Set of samples from incremental data flow.
        classes_weights : float[n_classes]
            Updated classes weight to use in igt/uls strategies.

        """
        ech = DaS().getEchantillon()
        self.__updateWeights(ech[1])
        return ech, self.classes_weights


    def __updateWeights(self, y):
        uniques, freq = np.unique(y, return_counts = True)
        self.classes_cpt[uniques] += freq.astype(np.uint64)
        self.classes_weights = Config().w_function[self.classes_cpt]




