# -*- coding: utf-8 -*-
"""
Created on Tue Dec  3 09:54:57 2019

@author: bourd

based on sklearn/tree/tree.py
"""


import numpy as np
import math
from Configuration import n_features
from scipy.stats import truncnorm
from sklearn.tree import DecisionTreeClassifier
#import pyximport; pyximport.install()
from mytree import MyTree
from Configuration import Config
MAX_INT = np.iinfo(np.int32).max


TREE_LEAF = -1
TREE_UNDEFINED = -2
CHILDREN_LEFT = 0
CHILDREN_RIGHT = 1
FEATURE = 2
THRESHOLD = 3
IMPURITY = 4
DEPTH = 5

class CustomTree(DecisionTreeClassifier):


    """
    Attributes of tree (inspired by scikit-learn tree_)
    ----------
    n_classes : int
        Number of classes
    n_nodes : int
        The number of nodes (internal nodes + leaves) in the tree.
    children_left : array of int, shape [node_count]
        children_left[i] holds the node id of the left child of node i.
        For leaves, children_left[i] == TREE_LEAF. Otherwise,
        children_left[i] > i. This child handles the case where
        X[:, feature[i]] <= threshold[i].
        < 0 if leaf
    children_right : array of int, shape [node_count]
        children_right[i] holds the node id of the right child of node i.
        For leaves, children_right[i] == TREE_LEAF. Otherwise,
        children_right[i] > i. This child handles the case where
        X[:, feature[i]] > threshold[i].
    feature : array of int, shape [node_count]
        feature[i] holds the feature to split on, for the internal node i.
        == TREE_UNDEFINED if leaf
    threshold : array of double, shape [node_count]
        threshold[i] holds the threshold for the internal node i.
        == TREE_UNDEFINED if leaf
    value : array of double, shape [node_count, max_n_classes]
        Contains the constant prediction value of each node.
    impurity : array of double, shape [node_count]
        impurity[i] holds the impurity (i.e., the value of the splitting
        criterion) at node i.
    """


    def __init__(self,
                max_depth = 20,
                max_features = 16,
                criterion="gini",
                splitter="best",
                min_samples_split=2,
                min_samples_leaf=1,
                min_weight_fraction_leaf=0.,
                max_leaf_nodes=None,
                min_impurity_decrease=0.,
                min_impurity_split=None,
                random_state = None,
                class_weight=None,
                presort='deprecated',
                ccp_alpha=0.0
                ):

        self.criterion=criterion
        self.splitter=splitter
        self.max_depth=max_depth
        self.min_samples_split=min_samples_split
        self.min_samples_leaf=min_samples_leaf
        self.min_weight_fraction_leaf=min_weight_fraction_leaf
        self.max_features=max_features
        self.max_leaf_nodes=max_leaf_nodes
        self.min_impurity_decrease=min_impurity_decrease
        self.min_impurity_split=min_impurity_split
        self.random_state = random_state
        self.class_weight=class_weight
        self.presort=presort
        self.ccp_alpha=ccp_alpha



        self.tree = None
        self.leaves_increment = dict()
        self.value_increment = None # effectif INCREMENT MANU
        self.values = None # effectifs TYPO + INCREMENT MANU



    def fit(self, X, y, sample_weight=None, check_input=True, X_idx_sorted=None):
        """
        shape(X) = [ n_samples, n_features ]
        shape(y) = [ n_samples ]
        """

        super().fit(X, y, None, check_input, X_idx_sorted)

        self.tree = MyTree(self.tree_.node_count, self.tree_.n_features)
        self.tree.setNodes(self.tree_.threshold, self.tree_.impurity,\
                    self.tree_.feature, self.tree_.children_left, self.tree_.children_right)

        self.values = self.tree_.value[:,0]
        self.n_classes = self.tree_.n_classes[0]
        self.value_increment = np.zeros(self.values.shape)
        del self.tree_

        ## get X path and add nodes' distributions
        #self.__feed_nodes(X, y)

    def predict(self, X, check_input=False):
        return np.array([ np.argmax(v) for v in self.values[self.tree.apply(X)]])

    def predict_proba(self, X, check_input=False):
        pred = self.predict(X)
        res = np.zeros((len(X), self.n_classes))
        for (it,p) in enumerate(pred):
            res[it][p] = 1.0
        return res

    def apply(self, X, check_input=False):
        return self.tree.apply(X)

"""
    Return the gini value of y_samples class distribution
    y_counts is the 1D array of classes occurences => shape [ n_classes ]
"""
def gini_counts(y_counts):
    counts_squarred = y_counts/sum(y_counts)
    return 1 - sum(counts_squarred**2)



"""
    Return the gini value of y_samples class distribution
    y_samples is a 1D array with shape [ n_samples ] containing each
    sample class
"""
def gini(y_samples):
    return gini_counts(np.unique(y_samples, return_counts = True)[1])


def toSTR(array):
    return np.array2string(array, separator=",",  formatter={'float_kind':lambda x: "%.5f" % x})[1:-1].replace("\n","").replace(" ","")

"""
    This static class allows to apply incremental training strategies
"""
class TreeUtils :


    @staticmethod
    def update_leaves(it, tree, mc, mc_lab, weights, dist):
        number_of_igt_applied = 0
        impurities = []
        for (x, y) in zip(mc, mc_lab):

            igt_applied, impurity = TreeUtils.update_leaf(tree, x, y, weights[y], dist)

            if igt_applied:
                number_of_igt_applied += 1
            impurities.append(impurity)
        return number_of_igt_applied, np.array(impurities)

    """
    GOAL :
        Apply ULS and then IGT if necessary

            ULS : Update classes distribution of leaves when incremental data
            arrive in

            IGT : expand tree by creating 1-depth-subtree where leaves impurity
            is bigger than Config.F
    ---------------------------------------------------------------------------
    INPUTS :
        t : MyTree
            is the tree to update with incremental data
        x : array[n_features]
            is the incremental example to add
        class_ : int
            is the class of x
        dist : dictionary (see LTM for more details)
            is the training data distribution

    ---------------------------------------------------------------------------
    OUTPUT :
        return False if IGT has not been applied, else True

    """
    @staticmethod
    def update_leaf(t, x, class_, w, dist):
        # get example arriving leaf
        x = x.reshape(1,-1)


        decision_path = t.tree.decision_path(x).indices
        leaf = decision_path[-1]


        # initialize incremental distribution for THIS leaf
        if leaf not in t.leaves_increment:
            #☻ one sample line = sample ([:-2]) + class ([-2]) + weight ([-1])
            t.leaves_increment[leaf] = np.empty((0, n_features + 2), dtype=np.float32)



        # THIS class which are arrived at this leaf before
        extended_x = np.array([np.concatenate((x.reshape(-1,), class_, w))])
        t.leaves_increment[leaf] = np.vstack((t.leaves_increment[leaf], extended_x))
        t.values[leaf][class_] += int(w)
        t.value_increment[leaf][class_] += int(w)


        #print("uls",time.time() - start)

        #update leaf impurity
        new_imp = gini_counts(t.values[leaf])


        # if IGT is necessary then apply it
        if new_imp > Config().F:
            t = TreeUtils.change_leaf_into_node(t, x, class_, decision_path, leaf, dist)

            return True, new_imp

        return False, new_imp


    """
        IGT

        R way : with truncated multi normal R package
    """

    @staticmethod
    def change_leaf_into_node(tree, x, y, decision_path, leaf, dist):

        generated_x_y =  np.empty((0, n_features + 1), dtype=np.float32) # contains generated base training data

        ''' 1) generate base training data with global dist '''
        N_learning = tree.values[leaf] - tree.value_increment[leaf] # contains base + incremental dist in leaf

        # for each class c generate nc samples using global distribution
        for (c,nc) in enumerate(N_learning):
            if nc > 0:
                means = dist[c][0]
#               R way : means_str = "c(" + toSTR(means) + ")"
                cov = np.diag(dist[c][1])
#               R way : cov_str = "c(" + toSTR(cov) + ")"
                generated_c_x = np.random.multivariate_normal(means, cov, int(nc))# remove if R way

#                R way : lower_str, upper_str = TreeUtils.getFeaturesLowerUpper(tree, decision_path)
#                R way : command = "sigma = diag(" + cov_str + ");x = rtmvnorm(n=" + str(int(nc)) +", mean=" + means_str + ", sigma=sigma, lower=" + lower_str + ", upper= " + upper_str + ", algorithm=\"gibbs\")"
#                R way : t = r(command)
#
#
#                R way : generated_c_x = r.get("x")[0:int(nc)]

                generated_c_x_y = np.concatenate((generated_c_x, np.array([[c]] * int(nc))), axis = 1)
                generated_x_y = np.append(generated_x_y, generated_c_x_y, axis = 0)




        ''' 2) retrieve incremental data with weights '''
        leaf_incr = tree.leaves_increment[leaf]
        #print("generate train and get incr",time.time() - start)

        generated_x_y = TreeUtils.correct_samples_with_previous_nodes(dist, tree.tree, generated_x_y, decision_path) # remove if R way

        n_left, n_right, node_thresh, node_feature = TreeUtils.build_subtree_from_leaf(tree,
                                                            leaf,
                                                            generated_x_y,
                                                            leaf_incr[:,:-1],
                                                            np.concatenate((np.array([1]*len(generated_x_y)), leaf_incr[:,-1].reshape(-1))) ## concatenation des poids des données régénérées (1) et des poids des données incrémentales (>= 1)

                                         )
        #print("build subtree",time.time() - start)
        # update leaves base and incremental data distributions
        f = node_feature
        t = node_thresh
        tree.leaves_increment[n_left] = leaf_incr[ leaf_incr[:, f] <= t]
        tree.leaves_increment[n_right] = leaf_incr[ leaf_incr[:, f] > t]

        tree.leaves_increment.pop(leaf, None)
        #print("update leaves_increment",time.time() - start)

        return tree


    @staticmethod
    def build_subtree_from_leaf(tree, leaf, generated_data, incremental_data, samples_weight):

        # expand tree by finding best split with generated data
        t = DecisionTreeClassifier(max_depth = 1, max_features = math.floor(math.sqrt(n_features)))

        samples_weight = samples_weight.reshape(-1).astype(int)

        X = np.vstack((generated_data[:,:-1], incremental_data[:, :-1]))

        classes = np.concatenate((generated_data[:,-1].reshape(-1), incremental_data[:, -1].reshape(-1)))
        t.fit(X, classes, sample_weight = samples_weight)

        # update leaf into node
        tree.tree.leaf_into_node(leaf, t.tree_.feature[0], t.tree_.threshold[0], t.tree_.impurity[0])

        # Convert 1-depth-decisionTree values for our tree before ADDING new leaves
        # indeed it is possible that X doesn't contain all classes so that
        # t tree values haven't the right size of n_classes

        val = t.tree_.value[1][0]
        c = np.array([0] * tree.n_classes)
        c[np.unique(classes.astype(np.uint8))] = val
        tree.values = np.vstack((tree.values, c))


        val = t.tree_.value[2][0]
        c = np.array([0] * tree.n_classes)
        c[np.unique(classes.astype(np.uint8))] = val
        tree.values = np.vstack((tree.values, c))


        n_left, n_right = TreeUtils.add_leaves(tree, leaf, t.tree_.impurity[1], t.tree_.impurity[2])

        leaves_dest = t.apply(incremental_data[:,:-1])

        c1 = np.array([0] * tree.n_classes)
        c2 = np.array([0] * tree.n_classes)

        for i in range(len(incremental_data)):
            if leaves_dest[i] == 1:
                c1[int(incremental_data[i,-1])] += samples_weight[len(generated_data) + i]
            else:
                c2[int(incremental_data[i,-1])] += samples_weight[len(generated_data) + i ]

        tree.value_increment = np.vstack((tree.value_increment, c1))
        tree.value_increment = np.vstack((tree.value_increment, c2))


        return  n_left, n_right, t.tree_.threshold[0], t.tree_.feature[0]


    @staticmethod
    def add_leaves(tree, parent, impurity_leaf_left, impurity_leaf_right):

        nodes = tree.tree.nodes

        nodes[parent, CHILDREN_LEFT] = tree.tree.node_count
        nodes[parent, CHILDREN_RIGHT] = tree.tree.node_count + 1

        node_left = np.array([TREE_LEAF, TREE_LEAF, TREE_UNDEFINED, TREE_UNDEFINED, impurity_leaf_left, nodes[parent, DEPTH] + 1])
        node_right = np.array([TREE_LEAF, TREE_LEAF, TREE_UNDEFINED, TREE_UNDEFINED, impurity_leaf_right, nodes[parent, DEPTH] + 1])

        nodes = np.vstack((nodes, node_left))
        nodes = np.vstack((nodes, node_right))

        tree.tree.nodes = nodes
        return int(nodes[parent, CHILDREN_LEFT]), int(nodes[parent, CHILDREN_RIGHT])

    """
    Le principe de cette méthode est de corriger la valeur des features des données régénrées.
    Cette correction est conditionnelle au chemin jusqu'à la feuille de l'igt en cours
    Pour cela, au niveau de chaque noeud et pour chaque exemple, on réalise la génération
    d'une nouvelle valeur pour la feature CONDITIONNELLEMENT au seuil.
    """
    @staticmethod
    def correct_samples_with_previous_nodes(dist, tree, XY, decision_path)    :


        next_node_is_right = tree.children_right[decision_path] == decision_path+1
        t = tree.threshold[decision_path].reshape(-1)
        f = tree.feature[decision_path].astype(int).reshape(-1)

        classes = XY[:,-1].astype(int)
        means = np.array([ dist[c][0] for c in range(10)])
        sigmas = np.array([ dist[c][1] for c in range(10)])
        # pour chacun des noeuds parents de la feuille
        for i in range(len(decision_path) - 1):
            # pouur chaque sample typographique généré  => corriger la feature du noeud courant si besoin
            #XY = [[sample_1, class1]
            #      [sample_2, class2]
            #        ....          ]]
            fi = f[i]
            ti = t[i]

            for j in range(XY.shape[0]):
                xji = XY[j,fi]
                c = classes[j]
                mean = means[c, fi]
                sigma = sigmas[classes[j],fi]

                # Si la valeur de la feature i pour l'exemple j est à droite du seuil
                # alors qu'elle devrait être à gauche (car le noeud suivant dans le chemin
                # jusqu'à la feuille est l'enfant gauche)
                # ----x ---- | --- o ----
                if xji > ti and not(next_node_is_right[i]) :
                    a = mean-sigma*5
                    if ti < a: # si sigma/10 est plus petit que notre valeur du pas (step)
                        XY[j,fi] = ti
                    else:
                        XY[j,fi] = truncnorm.rvs((a - mean)/sigma, (ti-mean)/sigma) * sigma + mean

                # idem si la valeur de feature est à guauche alors qu'elle devrait être à droite
                # ----o ---- | --- x ----
                elif xji <= ti and next_node_is_right[i]:
                    b = mean+sigma*5
                    if ti > b:
                        XY[j,fi] = ti + 0.01
                    else:
                       XY[j,fi] = truncnorm.rvs((ti - mean)/sigma, (b-mean)/sigma) * sigma + mean
        return XY

#    @staticmethod
#    def getFeaturesLowerUpper(tree, decision_path)    :
#        lower = np.array([-MAX_INT]*n_features)
#        upper = np.array([MAX_INT]*n_features)
#        # pour chacun des noeuds parents de la feuille
#        for i in range(len(decision_path) - 1):
#            Ni = decision_path[i]
#            next_node_is_right = tree.tree.children_right[Ni] == decision_path[i+1]
#            ti = tree.tree.threshold[Ni]
#            fi = int(tree.tree.feature[Ni])
#
#            if not(next_node_is_right) :
#                upper[fi] = min(upper[fi], ti)
#
#            elif next_node_is_right:
#                lower[fi] = max(lower[fi], ti)
#        lower_str = "c(" + toSTR(lower).replace(str(-MAX_INT), "-Inf") +")"
#        upper_str = "c(" + toSTR(upper).replace(str(MAX_INT), "Inf") + ")"
#        return lower_str, upper_str
#
#    @staticmethod
#    def maxDepth(tree):
#
#        # Create a empty queue for level order traversal
#        q = []
#
#        # Enqueue Root and Initialize Height
#        q.append(0)
#        height = 0
#
#        while(True):
#
#            # nodeCount(queue size) indicates number of nodes
#            # at current level
#            nodeCount = len(q)
#            if nodeCount == 0 :
#                return height - 1
#
#            height += 1
#
#            # Dequeue all nodes of current level and Enqueue
#            # all nodes of next level
#            while(nodeCount > 0):
#                node = q[0]
#                q.pop(0)
#                if tree.children_left[node] > -1:
#                    q.append(tree.children_left[node])
#                if tree.children_right[node] > -1:
#                    q.append(tree.children_right[node])
#
#                nodeCount -= 1
#
#